


//ngram combo
// Ext.define('NgramViewModel', {
// 	extend: 'Ext.data.Model',
// 	fields: [
// 		{type: 'string', name: 'ngram'}
// 	]
// });

// // The data store holding the states
// var ngramViewStore = Ext.create('Ext.data.Store', {
// 	model: 'NgramViewModel',
// 	data: [
// 		{ngram: '1'},
// 		{ngram: '2'},
// 		{ngram: '3'},
// 		{ngram: '4'},
// 		{ngram: '5'},
// 		{ngram: '6'},
// 		{ngram: '7'},
// 		{ngram: '8'},
// 		{ngram: '9'},
// 		{ngram: '10'}
// 	]
// });

// // Simple ComboBox using the data store
// var cmbNgramView = Ext.create('Ext.form.field.ComboBox', {
// 	fieldLabel: 'Ngarm',
// 	//anchor:'100%',
// 	//renderTo: 'simpleCombo',
// 	displayField: 'ngram',
// 	//width: 130,
// 	labelWidth: 35,
// 	store: ngramViewStore,
// 	queryMode: 'local',
// 	typeAhead: true
// //	listeners: {
// //    	render : {
// //            single : true,
// //            buffer : 100,
// //            fn     :function() {
// //                this.el.setWidth(70);
// //            }
// //        }
// //    }
// });

// cmbNgramView.on('select',function(combo, records, eOpts){
// 	//alert(JSON.stringify(records[0].get('action')));

// 	settings.set('currentNgram', records[0].get('ngram'))
// 	// viewSettings.currentNgram = records[0].get('ngram');

// 	// updateNgrams(viewSettings.selectedTabId);

// 	//trackEvent("ngramsToolbar.selectNgram",viewSettings.currentNgram);
// 	//showCloud();
// 	//alert(records[0].get('ngram'));
// });


// cmbNgramView.select(settings.get('currentNgram'));

Ext.define('Ext.ux.TagsCustomTrigger', { // clear search text
	extend: 'Ext.form.field.Trigger',
	alias: 'widget.tagscustomtrigger',
	selectOnFocus: true,
	initComponent: function () {
		var me = this;
  
		me.triggerCls = 'x-form-clear-trigger'; // native ExtJS class & icon
  
		me.callParent(arguments);
	},
	// override onTriggerClick
	onTriggerClick: function() {
		this.setRawValue('');
		// var store = tagsGrid.getStore(); //this.up().up().getStore();
		// store.loadData(getTagsPaneArr(''));
		this.focus(false, 300);
	}
  });

function createNgramsToolbar(){

	var toolbar = Ext.create('Ext.toolbar.Toolbar', {

		//width: '100%',
		// anchor: '100%',
		//autoWidth: true,
//		defaults: {
//            anchor: '100%'
//        },
		items: [
//		        {
//		        	 xtype: 'fieldcontainer',
//		        	 layout: {
//	                        type: 'hbox'
//	                        //defaultMargins: {top: 0, right: 5, bottom: 0, left: 0}
//	                    },
//		        	 items: [
//				        cmbNgramView,
				{
					xtype    : 'tagscustomtrigger', //'textfield',
					name     : 'txtTokenSearch',
					itemId   : 'txtTokenSearch',
					emptyText: 'search token',
					maxValue: 10000,
					selectOnFocus: true,
					listeners: {
					specialkey: function (field, e) {
						if (e.getKey() == e.ENTER) {
						//   var url = "https://www.google.com/search?q="+ tabsFilterGrid.dockedItems.items[1].items.items[0].getRawValue();
						//   chrome.windows.getCurrent(function(wnd) {
						// 	  chrome.tabs.create({"url": url}, function(tab) {

						// 	  });
						//   });

						}
					},
					change:{
						fn: function(that, newValue, oldValue, eOpts ){
							console.log("tagsGrid-grid textfield: " + newValue);
							// var store = tagsGrid.getStore();
							// store.loadData(getTagsPaneArr(newValue));
						}
					},
					afterrender: function(field) {
						field.focus(false, 300);
					}
					}
				},


				{
					xtype: 'numberfield',
					padding: '2 2 2 2',
					anchor:'100%',
					labelWidth: 35,
					//hideLabel: true,
					width : 90,
					fieldLabel: ' Length ',
					//name: 'basic',
					value: parseInt(settings.get('currentNgram')) || 2,
					minValue: 1,
					maxValue: 20,
					listeners: {
						change:{
								fn: function(that, newValue, oldValue, eOpts ){
									console.log(newValue);
									settings.set('currentNgram', newValue);
									updateNgrams();
								}
						}
					}
				},
				' ',
				{
					xtype: 'numberfield',
					padding: '2 2 2 2',
					anchor:'100%',
					labelWidth: 20,
					//hideLabel: true,
					width : 90,
					fieldLabel: ' Min',
					//name: 'basic',
					value: parseInt(settings.get('minNgram') || 2),
					minValue: 1,
					maxValue: 99999,
					listeners: {
						change:{
							fn: function(that, newValue, oldValue, eOpts ){
								console.log('minNgram newValue:', newValue);
								settings.set('minNgram', newValue);
								updateNgrams();
							}
						}
					}
				},
				' ',
			            {
			                xtype: 'numberfield',
			                padding: '2 2 2 2',
			                anchor:'100%',
			                labelWidth: 20,
			                width : 100,
			                fieldLabel: ' Max',
			                //name: 'basic',
			                value: parseInt(settings.get('maxNgram')) || 100000,
			                minValue: 1,
			                maxValue: 100000,
			                listeners: {
			                	change:{
			                    	fn: function(that, newValue, oldValue, eOpts ){
			                    		console.log(newValue);
			                    		settings.set('maxNgram', newValue);
										updateNgrams();
			                    	}
			                	}
//			            		,render : {
//			                        single : true,
//			                        buffer : 100,
//			                        fn     :function() {
//
//			                            this.el.setWidth(90);
//			                        }
//			                    }
			                }
			            }
//		            ]
//			    }
		    ]
		});
	return toolbar;


}
