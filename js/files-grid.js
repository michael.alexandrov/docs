

function createFilesGrid(arrFiles){
    var store = Ext.create('Ext.data.ArrayStore', {
	
		//sortInfo: { field: "count", direction: "DESC" },
		autoLoad: true,
		autoSync: true,
		fields: [
            {name: 'id',   type: 'int'},
            {name: 'name',  type: 'string'},
            {name: 'size',  type: 'int'},
            {name: 'date',  type: 'string'},

		],
		data: arrFiles
	});

    var grid = Ext.create('Ext.grid.Panel', {
		// plugins:[
		// 	Ext.create('Ext.grid.plugin.CellEditing', {
		// 	  clicksToEdit: 2
		// 	})
		// ],
        region: 'west',
	    store: store,
		title : "Files",

		collapsible: true,
		collapsed: true,

        selModel: { // select all checkboxes
			selType: 'checkboxmodel',
			showHeaderCheckbox: true,
			ignoreRightMouseSelection: true
		},

        // dockedItems: [createWindowsPaneToolbar()],
	    //height: '30%',
        flex: 1,
        //minHeight: 80,
      	split: true,
//	    autoRender: true,
	    autoScroll: true,
	    layout: 'fit',

        columns: [
        	{text: "ID", flex: 1,  dataIndex: 'id', sortable: true},
            {text: "Name", flex: 1,  dataIndex: 'name', sortable: true},
            {text: "Size", flex: 1,  dataIndex: 'size', sortable: true},
            {text: "Date", flex: 1,  dataIndex: 'date', sortable: true}
        ],

		listeners: {
        	selectionchange:{
            	fn: function( /*Ext.selection.Model*/ that, /* Ext.data.Model[]*/ selected,/* Object*/ eOpts ){

               
            		// alert(JSON.stringify(selected[0].data));
					console.log('selectionchange: ', JSON.stringify(selected[0].data));

					settings.set('selectedDocName', selected[0].data['name']);

            		if(selected.length > 0){
						sentencesGrid.setLoading(true, sentencesGrid.body);

						var sents_path = selected[0].data['name'] + '.sents.json';

						sents_json = cache.get(sents_path);
						if(sents_json){
							sentencesGrid.getStore().loadData(sents_json);
							sentencesGrid.setLoading(false, sentencesGrid.body);
						}
						else{
							getJsonData(sents_path, function(jsonData) {
								if (jsonData) {
								  console.log('Received JSON data:', jsonData);
								  // Update the store with the new data
								  sentencesGrid.getStore().loadData(jsonData);
								  sentencesGrid.setLoading(false, sentencesGrid.body);
								  cache.set(sents_path, jsonData);
								} else {
								  console.error('Failed to retrieve ' + sents_path);
								  sentencesGrid.setLoading(false, sentencesGrid.body);
								}
							});
						}
						

						pos_path = selected[0].data['name'] + '.pos.json';
						pos_json = cache.get(pos_path);
						if(pos_json){
							updateGrid(posGrid, pos_json);
						}
						else{
							getJsonData(pos_path, function(jsonData) {
								if (jsonData) {
								  console.log('Received JSON data:', pos_path);
								  cache.set(pos_path, jsonData);
								  updateGrid(posGrid, jsonData);
								  
								} else {
								  console.error('Failed to retrieve ' + pos_path);
								  posGrid.setLoading(false, posGrid.body);
								}
							});
						}

						updateNgrams();

						// Explain-taxonomy.txt.ngram-2.json

						// https://s3.us-west-2.amazonaws.com/michael.alexandrov/topics/Explain-taxonomy.txt.ngram-2.json

						

					}
				}
			}
		}

    });

    return grid;
}