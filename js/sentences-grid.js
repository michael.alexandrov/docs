

function createSentencesGrid(arrSents){
    var store = Ext.create('Ext.data.ArrayStore', {
	
		//sortInfo: { field: "count", direction: "DESC" },
		autoLoad: true,
		autoSync: true,
		fields: [
            {name: 'id',   type: 'int'},
            {name: 'sentence',  type: 'string'},
            {name: 'polarity',  type: 'float'},
            {name: 'subjectivity',  type: 'float'},

		],
		data: arrSents
	});

    var grid = Ext.create('Ext.grid.Panel', {
		// plugins:[
		// 	Ext.create('Ext.grid.plugin.CellEditing', {
		// 	  clicksToEdit: 2
		// 	})
		// ],
        region: 'east',
	    store: store,
		title : "Sentences",

        // selModel: { // select all checkboxes
		// 	selType: 'checkboxmodel',
		// 	showHeaderCheckbox: true,
		// 	ignoreRightMouseSelection: true
		// },

        // dockedItems: [createWindowsPaneToolbar()],
	    //height: '30%',
        flex: 1,
        //minHeight: 80,
      	split: true,
//	    autoRender: true,
	    autoScroll: true,
	    layout: 'fit',

        columns: [
        	{text: "ID", flex: 1,  dataIndex: 'id', sortable: true},
            {text: "Sentence", flex: 1,  dataIndex: 'sentence', sortable: true},
            {text: "Polarity", flex: 1,  dataIndex: 'polarity', sortable: true},
            {text: "Subjectivity", flex: 1,  dataIndex: 'subjectivity', sortable: true}
        ]

    });

    return grid;
}