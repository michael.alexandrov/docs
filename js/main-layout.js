Ext.require(['*']);
// Ext.Loader.setConfig({ enabled: true, paths: { 'Ext.ux': 'ext/src' } });
// Ext.require('Ext.ux.grid.FiltersFeature');

var mainView = null;

var fileGrid = createFilesGrid([{"id": 1, "name": "hello", "size": 0, "date": "today"},{"id": 2, "name": "hello2", "size": 0, "date": "today"}])

var sentencesGrid = createSentencesGrid([{"id": 1, "sentence": "hello", "polarity": 0, "subjectivity": 0}]);

var ngramsGrid = createNgramsGrid([{"str": "hello", "count": 1}]);

var posGrid = createPosGrid([{"str": "hello", "count": 1}]);

var files_list_json = [];

// nodesGraphPane = create3dGraphPanel();


Ext.onReady(function() {

	console.log("Ext.onReady(function()");

    mainView = Ext.create('Ext.Viewport', {
        layout: {
            type: 'border',
            padding: 5
        },
        defaults: {
            split: true
        },
        items: [
            fileGrid, sentencesGrid,
        
            {
                visible: false,
                region: 'north',

                title: '3D Graph',
                autoRender: true,
                flex: 2,
                autoScroll: true,
                // border: true,
                // layout: 'border',
                layout: 'fit',
                anchor: '100% 100%',
                split: true,
                collapsible: true,
                collapsed: false,

                dockedItems: [create3DNodeGraphToolbar()],
                html: '<div id="3d-graph"></div>'
                // region: "north",

                //	layout: 'border',
                    //split: true,
                    // items:[
                    // 	//tagsGrid, 
                    // 	nodesGraphPane
                    // ]
            },
            Ext.createWidget('tabpanel', {
                activeTab: 0,
                plain: true,
                region: "center",
                //height: '50%',
                flex: 5,
                minHeight: 80,
                split: true,
                defaults :{
                    autoScroll: true,
                    bodyPadding: 1
                },
                //tbar: [ngramsToolbar],
                // dockedItems: [createTabsIndexToolbar()],
                items: [ngramsGrid, posGrid]
            })
            
        ] 
        
        //, nodesGraphPane]

    })

    // var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Please wait..."});
    // myMask.show();
    
    fileGrid.setLoading(true, fileGrid.body);
    getJsonData('topics/files-list.json', function(jsonData) {
        if (jsonData) {
          console.log('Received JSON data:', jsonData);
          // Update the store with the new data
          files_list_json = jsonData;
          fileGrid.getStore().loadData(files_list_json);
          fileGrid.setLoading(false, fileGrid.body);
        } else {
            fileGrid.setLoading(false, fileGrid.body);
            console.error('Failed to retrieve topics/files-list.json.');
        }
    });

    graphMgr.createNodesGraph("3d-graph");
    update3DGraph();

   
})
