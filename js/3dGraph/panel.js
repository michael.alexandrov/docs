

function create3dGraphPanel(){
	console.log('create3dGraphPanel')
    var panel = Ext.create('Ext.panel.Panel', {
		//title: '3D Graph',
		autoRender: true,
		flex: 5,
		autoScroll: true,
		// border: true,
		// //layout: 'border',
		layout: 'fit',
		anchor: '100% 100%',
		split: true,
		// collapsible: true,
		// collapsed: false,

		region: "center",
         dockedItems: [create3DNodeGraphToolbar()],
		 html: '<div id="3d-graph"></div>'//,
		 //renderTo: Ext.getBody()
	 });
    return panel;
}