
function create3DNodeGraphToolbar(){

	var toolbar = Ext.create('Ext.toolbar.Toolbar', {

		//width: '100%',
		// anchor: '100%',
		//autoWidth: true,
//		defaults: {
//            anchor: '100%'
//        },
        items: [
            'Arrange by',
            {
                xtype: 'checkboxfield',
                boxLabel: 'Docs ',
                checked: settings.getJson('3DNodesGraph.linkDocs') || false,
                listeners: {
                  change: function( that, newValue, oldValue, eOpts ) {
                    console.log('checkboxfield change Domain item:' + newValue + ' eOpts: ' + eOpts);
                    settings.setJson('3DNodesGraph.linkDocs', newValue);
                    // tabNodesByWindow();
                      update3DGraph();
                    }
                }
            },
            {
                xtype: 'checkboxfield',
                boxLabel: 'Tokens ',
                checked: settings.getJson('3DNodesGraph.linkTokens') || false,
                listeners: {
                  change: function( that, newValue, oldValue, eOpts ) {
                    console.log('checkboxfield change Window item:' + newValue + ' eOpts: ' + eOpts);
                    settings.setJson('3DNodesGraph.linkTokens', newValue);
                    // tabNodesByWindow();
                      update3DGraph();
                    }
                }
            }
            // ,
            // {
            //     xtype: 'checkboxfield',
            //     boxLabel: 'Opener ',
            //     checked: settings.getJson('3DNodesGraph.linkByOpener') || false,
            //     listeners: {
            //       change: function( that, newValue, oldValue, eOpts ) {
            //         console.log('checkboxfield change Opener item:' + newValue + ' eOpts: ' + eOpts);
            //         settings.setJson('3DNodesGraph.linkByOpener', newValue);
            //         // tabNodesByWindow();
            //           update3DGraph();
            //         }
            //     }
                
            // },
            // ' | ',
            // {
            //     xtype: 'checkboxfield',
            //     boxLabel: ' Show Tags ',
            //     checked: settings.getJson('3DNodesGraph.linkByTags') || false,
            //     listeners: {
            //       change: function( that, newValue, oldValue, eOpts ) {
            //         console.log('checkboxfield change showTags item:' + newValue + ' eOpts: ' + eOpts);
            //         settings.setJson('3DNodesGraph.linkByTags', newValue);
            //         // tabNodesByWindow();
            //         update3DGraph();
            //         }
            //     }
            // }
            // ,      
			// 	        ' ',
			// 	        {
			//                 xtype: 'numberfield',
			// 								padding: '2 2 2 2',
			//                 anchor:'100%',
			//                 labelWidth: 20,
			//                 //hideLabel: true,
			//                 width : 90,
			//                 fieldLabel: ' Min',
			//                 //name: 'basic',
			//                 value: settings.getJson('indexPane.minTermsCount'),
			//                 minValue: 1,
			//                 maxValue: 10000,
			//                 listeners: {
			//                 	change:{
			//                     	fn: function(that, newValue, oldValue, eOpts ){
			//                     		console.log("indexPane.minTermsCount change newValue: " + newValue);
			// 							settings.setJson('indexPane.minTermsCount', newValue);
			// 							updateTabsIndexGrid();
			//                     	}
			//                 	}
			//                 }
			//             },
			//             ' ',
		    ]
		});
	return toolbar;

};


