
//const Graph = ForceGraph3D();

const Graph = ForceGraph3D({
    extraRenderers: [new THREE.CSS2DRenderer()]
});

const highlightNodes = new Set();
const highlightLinks = new Set();
let hoverNode = null;

var graphMgr = {
    addNode: function (node){
        console.debug("graphMgr.addNode node.id: " + node.id);
        var { nodes, links } = Graph.graphData();
        nodes.push(node);
        Graph.graphData({
            nodes: nodes,
            links: links
          });
          return graphMgr.getNode(node.id);
    },

    addLink: function (sourceId, targetId){
        console.debug("graphMgr.addLink sourceId: " + sourceId + " targetId: " + targetId);
        //Graph.graphData().links.push({ source: sourceId, target: targetId });
        var { nodes, links } = Graph.graphData();
        links.push({ "source": sourceId, "target": targetId, "value": 150 });
        Graph.graphData({
            nodes: nodes,
            links: links
          });
    },

    getNode: function (id){
        console.debug("graphMgr.gethNode id: " + id);

        
        const { nodes, links } = Graph.graphData();
        
    
        for(var i = 0; i < nodes.length; i++){
            if(id == nodes[i].id){
                console.debug("#found - graphMgr.gethNode node: " + id);
    
                return nodes[i];
            }
        }
       
        console.debug(">>> NOT FOUND - graphMgr.gethNode node: " + id);
        return null;
    },

    removeNode: function (node) {
        console.debug("graphMgr.removeNode node: " + node.id);
        var { nodes, links } = Graph.graphData();
        links = links.filter(l => l.source !== node && l.target !== node); // Remove links attached to node

        for(var i = 0; i < nodes.length; i++){
            if(node.id == nodes[i].id){
                nodes.splice(i, 1); // Remove node
                break;
            }
        }
        
        //nodes.forEach((n, idx) => { n.id = idx; }); // Reset node ids to array index
        Graph.graphData({ nodes, links });
    },

    selectNode: function (nodeId){
        console.debug("graphMgr.selectNode node: " + nodeId);
        var node = graphMgr.getNode(nodeId);
        if(node){
            console.debug("graphMgr.selectNode node: " + node.id);
            const distance = 100;
            const distRatio = 1 + distance/Math.hypot(node.x, node.y, node.z);
    
            Graph.cameraPosition(
                { x: node.x * distRatio, y: node.y * distRatio, z: node.z * distRatio }, // new position
                node, // lookAt ({ x, y, z })
                1000  // ms transition duration
            );
        }
        
    },

    updateHighlight: function () {
        // trigger update of highlighted objects in scene
        Graph
          .nodeColor(Graph.nodeColor())
          .linkWidth(Graph.linkWidth())
          .linkDirectionalParticles(Graph.linkDirectionalParticles());
    },

    createNodesGraph: function (elemId){
        const elem = document.getElementById(elemId);
    
        var nodesData = {
            "nodes": [
               {"id": "WndRoot", "group": 1},
                {"id": "DomainsRoot", "group": 1},
                
               {"id": "TabsRoot", "group": 1}
            ],
            "links": [
               
            ]
        };
    
       
        //console.log('from 3d-graph.js Graph: ' + Graph);

        
    
         Graph(elem)
            //.jsonUrl('../datasets/miserables.json')
            .graphData(nodesData)
            //.nodeLabel('text')
            .nodeAutoColorBy('group')

            // .nodeColor(node => highlightNodes.has(node) ? node === hoverNode ? 'rgb(255,0,0,1)' : 'rgba(255,160,0,0.8)' : 'rgba(0,255,255,0.6)')
            
            .linkWidth(link => highlightLinks.has(link) ? 4 : 1)
            .linkDirectionalParticles(link => highlightLinks.has(link) ? 4 : 0)
            .linkDirectionalParticleWidth(4)

            // .nodeThreeObject(node => {
            //     const sprite = new SpriteText(node.text || node.id);
            //     sprite.material.depthWrite = false; // make sprite background transparent
            //     sprite.color = node.color;
            //     sprite.textHeight = 5;
            //     return sprite;
            //   })

            .nodeThreeObject(node => {
                const nodeEl = document.createElement('div');
                //nodeEl.textContent = node.text || node.id;
                if(node.favIconUrl){
                    // <a href="javascript:alert(\'--Hello\');" onclick="alert(\'Hello\'); graphMgr.selectNode(\'' + node.id + '\'); return false;"> + '</a>'
                    nodeEl.innerHTML = '<img src="' + node.favIconUrl + '" width="20" height="20" >' + node.text || node.id ;
                }
                else{
                    // '<a href="javascript:alert(\'--Hello\');" onclick="alert(\'Hello\'); graphMgr.selectNode(\'' + node.id + '\'); return false;">' + + '</a>'
                    nodeEl.innerHTML =  node.text || node.id ;

                }

                // nodeEl.onclick = function(){
                //     alert("nodeEl");
                // };

                nodeEl.style.color = node.color;
                nodeEl.className = 'node-label';
                return new THREE.CSS2DObject(nodeEl);
            })
            .nodeThreeObjectExtend(true)

            .onNodeHover(node => {
                // no state change
                if ((!node && !highlightNodes.size) || (node && hoverNode === node)) return;
      
                highlightNodes.clear();
                highlightLinks.clear();
                if (node && node.neighbors) {
                  highlightNodes.add(node);
                  node.neighbors.forEach(neighbor => highlightNodes.add(neighbor));
                  node.links.forEach(link => highlightLinks.add(link));
                }
      
                hoverNode = node || null;
      
                graphMgr.updateHighlight();
            })
            .onLinkHover(link => {
                highlightNodes.clear();
                highlightLinks.clear();
        
                if (link) {
                    highlightLinks.add(link);
                    highlightNodes.add(link.source);
                    highlightNodes.add(link.target);
                }
        
                graphMgr.updateHighlight();
            })
            //.onNodeHover(node => elem.style.cursor = node ? 'pointer' : null)

            .onNodeClick(node => {
                // Aim at node from outside it
                graphMgr.selectNode(node);
            });
    
            nodesData.links.forEach(link => {
                const a = graphMgr.getNode(link.source);
                const b = graphMgr.getNode(link.target);
                if(a && b){
                    !a.neighbors && (a.neighbors = []);
                    !b.neighbors && (b.neighbors = []);
                    a.neighbors.push(b);
                    b.neighbors.push(a);
            
                    !a.links && (a.links = []);
                    !b.links && (b.links = []);
                    a.links.push(link);
                    b.links.push(link);
                }
            });
    }
};



function tabNodesByWindow(){
    console.debug("tabNodesByWindow");
    //var tabsArr = getTabsArr();
    //var tabs = chrome.extension.getBackgroundPage().allTabs;

    var linkByTabs = settings.getJson('3DNodesGraph.linkByOpener');
    var linkByWindows = settings.getJson('3DNodesGraph.linkByWindow');
    var linkByDomains = settings.getJson('3DNodesGraph.linkByDomain');
    var linkByTLDs = settings.getJson('3DNodesGraph.linkByTLD') || true;

    var linkByTags = settings.getJson('3DNodesGraph.linkByTags');


    var tabs = Array.from(chrome.extension.getBackgroundPage().allTabs.values());

    var { nodes, links } = Graph.graphData();
    nodes = [
     //   {"id": "WndRoot", "group": 1},
       // {"id": "DomainsRoot", "group": 1},
        // {"id": "Filters", "group": 1},
      //  {"id": "TabsRoot", "group": 1}
    ]
    links = [];

    if(linkByWindows){
            nodes.push({"id": "WndRoot", "text": "Windows", "group": 1});
    }

    if(linkByDomains){
        nodes.push({"id": "DomainsRoot", "group": 1});
    }

    if(linkByTags){
        var tags = settings.getJson('3DNodesGraph.tags');

        nodes.push({"id": "tag_Tags", "text": "Tags", "group": 1});
        // if(!graphMgr.getNode("tag_Tags")){
        //    
        // }
        
        for(var tag in tags) {
            nodes.push({ "id": "tag_" + tag, "text": tag, "group": 2 });
            links.push({ "source": "tag_Tags", "target": "tag_" + tag });
        }
    }

    if(linkByTabs){
        nodes.push({"id": "TabsRoot", "group": 1});
    
    }

    var wnds = [];
    var domains = [];

    // Add and link tab nodes
    for(var i = 0; i < tabs.length; i++){
        tab = tabs[i];
        console.debug("tabNodesByWindow: id:" + tab.id);
        console.debug("tabNodesByWindow: windowId:" + tab.windowId);
        console.debug("tabNodesByWindow: openerTabId:" + tab.openerTabId);
        console.debug("tabNodesByWindow: title:" + tab.title);
        //console.debug("tabNodesByWindow: --> " + JSON.stringify(tab));

        
        nodes.push({
            "id": "tab_" + tab.id,
            "windowId": tab.windowId,
            "openerTabId": tab.openerTabId,
            "text": tab.title,
            "favIconUrl": tab.favIconUrl,
            "group": 3
        });

        if(linkByWindows){

            if(!wnds.includes("wnd_" + tab.windowId)){
                wnds.push("wnd_" + tab.windowId)
            }

            links.push({
                "source": "wnd_" + tab.windowId, 
                "target": "tab_" + tab.id,
                "value": 300
            });
        }
        
        if(linkByTabs){
            if(tab.openerTabId){
                links.push({
                    "source": "TabsRoot", 
                    "target": "tab_" + tab.openerTabId
                });
                links.push({
                    "source": "tab_" + tab.openerTabId, 
                    "target": "tab_" + tab.id
                });
            }
            else{
                links.push({
                    "source": "TabsRoot", 
                    "target": "tab_" + tab.id
                });
            }
        }

        if(linkByDomains){
            //nodes.push({"id": "DomainsRoot", "group": 1});
            var url = new URL(tab.url);

            if(linkByTLDs){
                var domainParent = "DomainsRoot";

                
                var subDomains = url.hostname.split(".").reverse();
                var domainName = subDomains[0];

                links.push({
                    "source": "DomainsRoot", 
                    "target": "domain_" + domainName,
                    "value": 500
                });  

                if(!domains.includes(domainName)){
                    domains.push(domainName);
                }

                
                // links.push({
                //     "source": domainParent, 
                //     "target": "domain_" + domainName
                // });

                domainParent = "domain_" + domainName; 

                for(var j = 1; j < subDomains.length; j++){
                    domainName =  subDomains[j] + "." + domainName;
                    links.push({
                        "source": domainParent, 
                        "target": "domain_" + domainName
                    });
                    
                    
                    if(!domains.includes(domainName)){
                        domains.push(domainName);
                    }

                    domainParent = "domain_" + domainName;  
                }

                links.push({
                    "source": "domain_" + domainName, 
                    "target": "tab_" + tab.id
                });
            }
            else{
                if(!domains.includes(url.hostname)){
                    domains.push(url.hostname);
                }
                links.push({
                    "source": "domain_" + url.hostname, 
                    "target": "tab_" + tab.id
                });
            } 
        }

        if(linkByTags){
            
           

            if(tags){
                for(var tag in tags) {
                    if(tags[tag].active){
                        
                        var linkToTab = false;
                        if(tags[tag].search.title ){
                            if(tab.title.toLowerCase().indexOf(tag) > -1){
                                linkToTab = true;
                            }
                       }

                       if(!linkToTab && tags[tag].search.url){
                            if(tab.url.toLowerCase().indexOf(tag) > -1){
                                linkToTab = true;
                            }
                        }
            
                        if(!linkToTab && tags[tag].search.text){
                            if(tabs[i].text.toLowerCase().indexOf(tag) > -1){
                                linkToTab = true;
                            }
                        }

                        if(linkToTab){
                            links.push({
                                "source": "tag_" + tag, 
                                "target": "tab_" + tab.id
                            });
                        }

                    }
                    
               }
            }
            else{
                console.error(">>> MISSING !!! - tags = settings.getJson('3DNodesGraph.tags')")
            }
        }
    }

    // Add and link windows nodes
    for(var i = 0; i < wnds.length; i++){
        wndId = wnds[i];
        console.debug("tabNodesByWindow: wndId:" + wndId);
        nodes.push({
            "id": wndId,
            "text": wndId,
            "group": 2
        });

        links.push({
            "source": "WndRoot", 
            "target": wndId,
            "value": 500
        });    
    }

    // Add and link domain nodes
    for(var i = 0; i < domains.length; i++){
        domain = domains[i];
        console.debug("tabNodesByWindow: domain:" + domain);
        nodes.push({
            "id": "domain_" + domain,
            "text": domain,
            "group": 4
        });

        // links.push({
        //     "source": "DomainsRoot", 
        //     "target": domain,
        //     "value": 500
        // });    
    }

    console.debug("tabNodesByWindow: nodes --> " + JSON.stringify(nodes));
    console.debug("tabNodesByWindow: links --> " + JSON.stringify(links));

    Graph.graphData({
        nodes: nodes,
        links: links
    });

    links.forEach(link => {
        const a = graphMgr.getNode(link.source);
        const b = graphMgr.getNode(link.target);
        if(a && b){
            !a.neighbors && (a.neighbors = []);
            !b.neighbors && (b.neighbors = []);
            a.neighbors.push(b);
            b.neighbors.push(a);

            !a.links && (a.links = []);
            !b.links && (b.links = []);
            a.links.push(link);
            b.links.push(link);
        }  
    });

}

function updateTabNodes(){
    tabNodesByWindow();
}



// function updateNodesGraph(){

//     console.debug("updateNodesGraph started");

//     var tabsFilterGrid = chrome.extension.getBackgroundPage().gettabsFilterView().tabsFilterGrid;
// 	var selectedRow = tabsFilterGrid.getSelectionModel().getSelection()[0];

//     tabData = chrome.extension.getBackgroundPage().allTabs.get(selectedRow.get("id"));

//     console.debug("updateNodesGraph tabData: " + JSON.stringify( tabData));

// 	var pageLinks = tabData.links;

//     var tabNode = graphMgr.getNode("tab_" +  tabData.id.toString());

//     console.debug("updateNodesGraph tabNode: " + JSON.stringify( tabNode));

//     if(!tabNode){
//         tabNode = graphMgr.addNode({
//             "id": "tab_" + tabData.id.toString(),
//             "text": tabData.title,
//             "group": 1
//         });
//     }

//     var linksNode = graphMgr.getNode("tab_" +  tabNode.id + "_links");
//     if(!linksNode){
//         linksNode = graphMgr.addNode({
//             "id": "tab_" +  tabNode.id + "_links",
//             "text": "Links",
//             "group": 1
//         });
//     }

//     var textNode = graphMgr.getNode("tab_" +  tabNode.id + "_text");
//     if(!textNode){
//         textNode = graphMgr.addNode({
//             "id": "tab_" +  tabNode.id + "_text",
//             "text": "Index",
//             "group": 1
//         });
//     }

    
//     var { nodes, links } = Graph.graphData();

//     links.push({ "source": "Count", "target": tabNode.id, "value": 150});
//     links.push({ "source": tabNode.id, "target": linksNode.id, "value": 150});
//     links.push({ "source": tabNode.id, "target": textNode.id, "value": 150});

//     // console.debug("updateNodesGraph nodes: " + JSON.stringify( nodes));

//     // console.debug("updateNodesGraph links: " + JSON.stringify( links));

//     var linksText = "";

//     for(var i = 0; i < pageLinks.length; i++){
//         linksText += pageLinks[i].text + "\n";
//     }

//     // add tokens nodes
//     // var ngramer = new Ngramer(linksText,[]);
//     // var tokensCount = ngramer.getTokensCount(1,2,5000);
//     // var filteredTokens = new Ext.util.HashMap();

//     // tokensCount.each(function(key, value, length){
//     //     if(value >= 2 && value <= 500){
//     //         if(!ngramer.isStopWord(key)){
//     //             filteredTokens.add(key,value);
//     //             nodes.push({"id": key, "text": key, "group": 3, "val": value});
//     //             links.push({ "source": textNode.id, "target": key, "value": 150});
//     //         }           
//     //     }
//     // });


//     // var tokensCount2 = ngramer.getTokensCount(2,2,5000);
//     // var filteredTokens2 = new Ext.util.HashMap();
//     // tokensCount2.each(function(key, value, length){
//     //     if(value >= 2 && value <= 500){
//     //         if(!ngramer.isStopWord(key)){
//     //             filteredTokens2.add(key,value);
//     //             nodes.push({"id": key, "text": key, "group": 4, "val": value});
                
//     //             var arr = key.split(" ");
//     //             for(var i = 0; i < arr.length; i++){
//     //                 if(filteredTokens.containsKey(arr[i])){
//     //                     links.push({ "source": arr[i], "target": key, "value": 150});
//     //                 }
//     //             }
//     //         }
            
//     //     }
//     // });


//     for(var i = 0; i < pageLinks.length; i++){ //pageLinks.length
//         if(pageLinks[i].href && pageLinks[i].href.length > 0 
//             && pageLinks[i].text && pageLinks[i].text.length > 0){
//             console.debug("updateNodesGraph nodes.push(: " + JSON.stringify( pageLinks[i]));

//             nodes.push({"id": pageLinks[i].href, "text": pageLinks[i].text, "group": 2}); //.substring(0,30)
//             //links.push({ "source": linksNode.id, "target": pageLinks[i].href, "value": 150});

//             var arr = pageLinks[i].text.split(" ");
//             for(var j = 0; j < arr.length; j++){
//                 if(filteredTokens.containsKey(arr[j])){
//                     links.push({ "source": arr[j], "target": pageLinks[i].href, "value": 150});
//                 }
//             }
//             // graphMgr.addNode({"id": pageLinks[i].href, "text": pageLinks[i].text.substring(0,30), "group": 2});
//             // graphMgr.addLink(tabNode.id, pageLinks[i].href)
//         }
// 	}

   
   
//     //const elem = document.getElementById("3d-graph");
//     Graph.graphData({
//         nodes: nodes,
//         links: links
//     });

//     links.forEach(link => {
//         const a = graphMgr.getNode(link.source);
//         const b = graphMgr.getNode(link.target);
//         if(a && b){
//             !a.neighbors && (a.neighbors = []);
//             !b.neighbors && (b.neighbors = []);
//             a.neighbors.push(b);
//             b.neighbors.push(a);

//             !a.links && (a.links = []);
//             !b.links && (b.links = []);
//             a.links.push(link);
//             b.links.push(link);
//         }  
//     });
//     // .nodeLabel('text')
//     // .nodeAutoColorBy('group')
//     // .nodeThreeObject(node => {
//     //     const sprite = new SpriteText(node.text || node.id);
//     //     sprite.material.depthWrite = false; // make sprite background transparent
//     //     sprite.color = node.color;
//     //     sprite.textHeight = 8;
//     //     return sprite;
//     // })
//     // .d3Force('charge').strength(-120);

//     //Graph.d3Force('charge').strength(-120);

//     // tabNode = graphMgr.getNode("tab_" +  tabData.id.toString());

//     // console.debug("updateNodesGraph before graphMgr.selectNode tabNode: " + JSON.stringify( tabNode));

//     // graphMgr.selectNode(tabNode);

    
// }


// function addGraphNodes(arr){
//     var { nodes, links } = Graph.graphData();
//     nodes = nodes.concat(arr);
//     Graph.graphData({
//         nodes: nodes,
//         links: links
//       });
//       return graphMgr.getNode(node.id);
// }



// function addGraphLinks(arr){
//     //Graph.graphData().links.push({ source: sourceId, target: targetId });
//     var { nodes, links } = Graph.graphData();
//     links = links.concat(arr);
//     Graph.graphData({
//         nodes: nodes,
//         links: links
//       });
// }

