function updateGrid(grid, data) {

  grid.setLoading(true, grid.body);
    // Get the object keys from the first data item
    var keys = Object.keys(data[0]);
  
    // Create an array of column configurations dynamically
    var columns = keys.map(function(key) {
      return {
        text: key.charAt(0).toUpperCase() + key.slice(1), // Capitalize the column header
        dataIndex: key,
        flex: 1
      };
    });
  
    // Create an array of field configurations dynamically
    var fields = keys.map(function(key) {
      var fieldType = getFieldType(key, data);
      return { name: key, type: fieldType };
    });
  
    // Create a store
    var store = Ext.create('Ext.data.Store', {
      fields: fields,
      data: data
    });
  
    // Create an array of filter configurations dynamically
    var filters = keys.map(function(key) {
      var filterOptions = store.collect(key, false, true);
      var filterType = getFilterType(key, data);
      return { type: filterType, dataIndex: key, options: filterOptions };
    });
  
    // Update grid configuration
    grid.reconfigure(store, columns);
  
    // Update filter configurations
    var filtersFeature = grid.getPlugin('gridfilters');
    if (filtersFeature) {
      filtersFeature.clearFilters();
      filtersFeature.menuFilterText = 'Filters';
      filtersFeature.filters = filters;
      filtersFeature.createFilters();
    }
  
    // Load data into the grid
    store.loadData(data);

   
    grid.setLoading(false, grid.body);
}
  
function getFieldType(key, data) {
    // Find the first non-null value for the key in the data
    var value = data.find(function(item) {
      return item[key] !== null && item[key] !== undefined;
    })[key];
  
    // Determine the data type based on the value
    if (typeof value === 'string') {
      return 'string';
    } else if (typeof value === 'number') {
      return 'number';
    } else if (typeof value === 'boolean') {
      return 'boolean';
    } else if (value instanceof Date) {
      return 'date';
    }
  
    // Default to 'auto' if the type cannot be determined
    return 'auto';
  }

  function getFilterType(key, data) {

    /*
    In ExtJS, there are several built-in filter types available for grids. Here is a list of common filter types that can be used with the `filters` feature in ExtJS grid:

1. 'string': This filter type allows filtering based on a string value. It provides options such as 'contains', 'does not contain', 'starts with', 'ends with', and 'equals' to filter the grid data.

2. 'numeric': This filter type is used for numeric filtering. It provides options such as 'equals', 'not equals', 'less than', 'greater than', 'less than or equal to', 'greater than or equal to', 'between', and 'empty' to filter numeric values.

3. 'date': The 'date' filter type allows filtering based on date values. It supports options such as 'on', 'before', 'after', 'between', 'this month', 'this year', 'empty', and more to filter dates.

4. 'list': This filter type provides a dropdown list of predefined options to filter data. It allows selecting one or multiple options to include or exclude from the filtered results.

5. 'boolean': The 'boolean' filter type is specifically for filtering boolean (true/false) values. It provides options like 'yes', 'no', and 'empty' to filter boolean data.

6. 'listMenu': This filter type displays a menu with checkboxes for each unique value in the column. It allows selecting multiple values to filter the grid data.

7. 'numericMenu': This filter type shows a menu with numeric input fields to define custom filtering criteria, such as 'greater than', 'less than', 'equals', etc.

8. 'dateMenu': The 'dateMenu' filter type displays a menu with a date picker to choose a specific date or date range for filtering.

These filter types offer various filtering options and are used to filter data based on different data types and requirements. You can choose the appropriate filter type based on the data you are working with and the filtering behavior you want to achieve in your ExtJS grid.
    */
    // Get the field type for the key
    var fieldType = getFieldType(key, data);
  
    // Map the field type to the corresponding filter type
    switch (fieldType) {
      case 'string':
        return 'string';
      case 'number':
        return 'numeric';
      case 'boolean':
        return 'boolean';
      case 'date':
        return 'date';
      default:
        return 'auto';
    }
  }
  


function createPosGrid(arrData){
    // Get the object keys from the first data item
    var keys = Object.keys(arrData[0]);

    // Create an array of column configurations dynamically
    var columns = keys.map(function(key) {
      return {
        text: key.charAt(0).toUpperCase() + key.slice(1), // Capitalize the column header
        dataIndex: key,
        flex: 1
      };
    });

    // Create an array of field configurations dynamically
    var fields = keys.map(function(key) {
      var fieldType = typeof arrData[0][key];
      return { name: key, type: fieldType };
    });

    // Create a store
    var store = Ext.create('Ext.data.Store', {
      fields: fields,
      data: arrData
    });

    // Create an array of filter configurations dynamically
    // var filters = keys.map(function(key) {
    //     var filterOptions = store.collect(key, false, true);
    //     return { type: 'list', dataIndex: key, options: filterOptions };
    //   });

    // Create a grid panel
    var grid = Ext.create('Ext.grid.Panel', {
        autoScroll: true,
	    layout: 'fit',
        store: store,
        columns: columns
        // features: [{
        //     ftype: 'filters',
        //     encode: false,
        //     local: true,
        //     filters: filters
        //   }]
    });

    return grid;
}