
// Create a model based on the JSON data
Ext.define('MainGridModel', {
  extend: 'Ext.data.Model',
  fields: [
    'id',
    'name',
    'age'
    // Add more fields as needed based on your JSON structure
  ]
});

var main_grid_store = Ext.create('Ext.data.Store', {
    model: 'MainGridModel',
    data: [
      { "id": 1, "name": "Michael", "age": 30 },
      { "id": 2, "name": "Bob", "age": 40 }
    ]
});


function getJsonData(url, callback) {
    Ext.Ajax.request({
      url: url,
      method: 'GET',
      success: function(response) {
        var jsonData = Ext.decode(response.responseText);
        callback(jsonData);
      },
      failure: function(response) {
        console.error('Failed to retrieve JSON data:', response);
        callback(null);
      }
    });
}

function displayGridView(json_path) {
    
  
    // json_data = getJsonData
    // Create a store using the model and JSON data
    
  
    // Create the grid view
    var grid = Ext.create('Ext.grid.Panel', {
      store: main_grid_store,
      columns: [
        { text: 'ID', dataIndex: 'id' },
        { text: 'Name', dataIndex: 'name' },
        { text: 'Age', dataIndex: 'age' }
        // Add more columns as needed based on your JSON structure
      ],
      height: 400,
      width: 600,
      renderTo: Ext.getBody()
    });
  
    // Show the grid view
    grid.show();

    // getJsonData('data.json', function(jsonData) {
    //   if (jsonData) {
    //     console.log('Received JSON data:', jsonData);
    //     // Update the store with the new data
    //     store.loadData(jsonData);
    //   } else {
    //     console.error('Failed to retrieve JSON data.');
    //   }
    // });


}


function updateFilesNodes(){
  console.log('update3DGraph');

  var { nodes, links } = Graph.graphData();
    nodes = [
     //   {"id": "WndRoot", "group": 1},
       // {"id": "DomainsRoot", "group": 1},
        // {"id": "Filters", "group": 1},
      //  {"id": "TabsRoot", "group": 1}
    ]
    links = [];

    // for(file_item in files_list_json){
    for (let i = 0; i < files_list_json.length; i++) {
      console.log('update3DGraph' + JSON.stringify(files_list_json[i]));
      // graphMgr.addNode({"id": file_item["name"], "text": file_item["name"], "group": 1});

      nodes.push({
        "id": files_list_json[i]["name"], 
        "text": files_list_json[i]["name"], 
        "group": 1});
    }

    Graph.graphData({
      nodes: nodes,
      links: links
  });
    
}



function update3DGraph(){
  console.log('update3DGraph');

  var linkDocs = settings.getJson('3DNodesGraph.linkDocs');
  var linkTokens = settings.getJson('3DNodesGraph.linkTokens');

  var { nodes, links } = Graph.graphData();
  nodes = [
    {"id": "Docs", "group": 1},
    //  {"id": "DomainsRoot", "group": 1},
    //   {"id": "Filters", "group": 1},
    //  {"id": "TabsRoot", "group": 1}
  ]
  links = [];

  updateFilesNodes();

  // nodes.push({"id": "WndRoot", "text": "Windows", "group": 1});
  // nodes.push({"id": "Windows1", "text": "Windows1", "group": 1});
  // nodes.push({"id": "Windows2", "text": "Windows2", "group": 1});

  // console.log(nodes);

  // Graph.graphData({
  //     nodes: nodes,
  //     links: links
  // });
}

function filterByCount(array, minCount, maxCount) {
  return array.filter(obj => obj.count >= minCount && obj.count <= maxCount);
}

function updateNgrams(){

  selectedDocName = settings.get('selectedDocName');
  currentNgram = settings.get('currentNgram') || 1;

  minNgram = settings.get('minNgram') || 2;
  maxtNgram = settings.get('maxtNgram') || 100000;

  json_path = selectedDocName + '.ngram-' + currentNgram.toString() + '.json';

  ngramsGrid.setLoading(true, ngramsGrid.body);

  var docNgrams = cache.get(json_path);

  
  
  if (docNgrams){
    docNgrams = filterByCount(docNgrams, minNgram, maxtNgram);
    ngramsGrid.getStore().loadData(docNgrams);
    ngramsGrid.setLoading(false, ngramsGrid.body);
  }
  else{
    getJsonData(json_path, function(jsonData) {
      if (jsonData) {
        console.log('Received JSON data:', jsonData);
        cache.set(json_path, jsonData);
        
        jsonData = filterByCount(jsonData, minNgram, maxtNgram);

        ngramsGrid.getStore().loadData(jsonData);

        ngramsGrid.setLoading(false, ngramsGrid.body);
      } else {
        console.error('Failed to retrieve ' + json_path);
        ngramsGrid.getStore().loadData([]);
        ngramsGrid.setLoading(false, ngramsGrid.body);
      }
    });
  } 

  
}

function sumCounts(arrays) {
  const countsMap = new Map();

  // Iterate through each array in the input arrays
  for (const array of arrays) {
    // Iterate through each object in the current array
    for (const obj of array) {
      const { str, count } = obj;
      
      // Check if the str already exists in the countsMap
      if (countsMap.has(str)) {
        // If it exists, add the count to the existing value
        countsMap.set(str, countsMap.get(str) + count);
      } else {
        // If it doesn't exist, set the count as the initial value
        countsMap.set(str, count);
      }
    }
  }

  // Convert the countsMap to an array of objects
  const result = Array.from(countsMap, ([str, count]) => ({ str, count }));

  return result;
}
