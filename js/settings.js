
var settings = {

    set: function (key,value){
        localStorage.setItem(key,value);
    },
    setJson: function (key,value){
        settings.set(key,JSON.stringify(value));
    },
    get: function (key){
        var val = localStorage.getItem(key) || '';
        console.log('settings.get key: ' + key ); // + ' val: ' + val
        return val;
    },
    getJson: function (key){
        var val = settings.get(key)
        if (val && val.length > 0){
            return JSON.parse(val);
        }
        else{
            return '';
        }
    },

    remove: function(key){
        localStorage.removeItem(key);
    }
};

var cache = {
    dict: {},

    set: function (key,value){
        cache.dict[key] = value;
    },
    
    get: function (key){
        return cache.dict[key] || "";
    }

};
