import argparse
import boto3
import json

def get_ssm_parameters(json_file_path, include=None, exclude=None, prefix=None):
    # Load JSON file
    with open(json_file_path) as json_file:
        data = json.load(json_file)

    # Connect to AWS SSM
    ssm = boto3.client('ssm')

    # Get parameters
    parameters = []
    for param in data:
        name = param['Name']
        if include and name not in include:
            continue
        if exclude and name in exclude:
            continue
        if prefix and not name.startswith(prefix):
            continue

        response = ssm.get_parameter(Name=name, WithDecryption=True)

        # Add parameter details to the list
        parameters.append({
            'Name': name,
            'Value': response['Parameter']['Value'],
            'Type': response['Parameter']['Type']
        })

    # Save parameters to a file
    output_file_path = 'parameters_output.json'
    with open(output_file_path, 'w') as output_file:
        json.dump(parameters, output_file, indent=4)

    print(f'Successfully saved parameters to {output_file_path}')

def set_ssm_parameters(json_file_path):
    # Load JSON file
    with open(json_file_path) as json_file:
        data = json.load(json_file)

    # Connect to AWS SSM
    ssm = boto3.client('ssm')

    # Update parameters
    for param in data:
        name = param['Name']
        value = param['Value']
        ssm.put_parameter(Name=name, Value=value, Type='SecureString', Overwrite=True)

    print('Successfully updated parameters')

def delete_ssm_parameters(json_file_path):
    # Load JSON file
    with open(json_file_path) as json_file:
        data = json.load(json_file)

    # Connect to AWS SSM
    ssm = boto3.client('ssm')

    # Delete parameters
    for param in data:
        name = param['Name']
        ssm.delete_parameter(Name=name)

    print('Successfully deleted parameters')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='AWS SSM Parameter Management')
    parser.add_argument('operation', choices=['get', 'set', 'delete'], help='Operation to perform (get, set, or delete)')
    parser.add_argument('json_file_path', help='Path to the JSON file containing parameter details')
    parser.add_argument('--include', nargs='+', help='Include specific parameter names')
    parser.add_argument('--exclude', nargs='+', help='Exclude specific parameter names')
    parser.add_argument('--prefix', help='Filter parameters by prefix')
    args = parser.parse_args()

    if args.operation == 'get':
        get_ssm_parameters(args.json_file_path, args.include, args.exclude, args.prefix)
    elif args.operation == 'set':
        set_ssm_parameters(args.json_file_path)
    elif args.operation == 'delete':
        delete_ssm_parameters(args.json_file_path)
