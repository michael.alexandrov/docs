import os
import sys
import argparse
from modules.utils.TextUtils import detect_language
from modules.utils.Storage import Storage
from modules.SpacyProcessor import SpacyProcessor
from modules.TextProcessor import TextProcessor


def process_file(file_path):
    text = storage.read_file(file_path)

    lang = detect_language(text)
    print('txt-sents process_file: ', file_path, ' - ', lang)

    spacy_proc = SpacyProcessor(lang=lang)

    # convert the input data to a CSV format
    # output_data = input_data.replace('\n', ',')

    csv_path = file_path + '.sents.csv'

    # write the output data to either the specified output file or to standard output
    if args.output_csv:
       csv_path = args.output_csv
   

    spacy_proc.update_sentences(csv_path,text)

    spacy_proc.update_pos_csv(text, file_path + '.pos.csv')

    text_proc = TextProcessor('')

    # text_proc.get_file_path(content_dir, src, 'text/text.txt')

    text_proc.update_file_ngrams(file_path, save_json=True)


def process_directory(directory_path):
    print('txt-sents process_directory: ', directory_path)
    for file_name in os.listdir(directory_path):
        if file_name.endswith(".txt"):
            print('txt-sents process_directory file_name: ', file_name)
            file_path = os.path.join(directory_path, file_name)
            process_file(file_path)
    

if __name__ == '__main__':

    storage = Storage()

    parser = argparse.ArgumentParser(description='Convert a text file to a CSV file.')
    parser.add_argument('input_file', help='path to input text file')
    parser.add_argument('--output_csv', help='path to output CSV file')
    args = parser.parse_args()

    print('txt-sents.py: ', args.input_file)

    # read the contents of the input file
    # with open(args.input_file, 'r') as file:  
    # 

    if os.path.isdir(args.input_file):
        process_directory(args.input_file)
    elif os.path.isfile(args.input_file):
        process_file(args.input_file)
    else:
        print(f"Invalid path: {args.input_file}")
    
