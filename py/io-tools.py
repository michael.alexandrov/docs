import os
import boto3


def upload_directory_to_s3(directory, bucket_name, include_extensions=None, exclude_extensions=None):
    """
    Recursively uploads the contents of a directory to an Amazon S3 bucket.

    Args:
        directory (str): The local directory path.
        bucket_name (str): The name of the Amazon S3 bucket.
        include_extensions (list): List of file extensions to include (optional).
        exclude_extensions (list): List of file extensions to exclude (optional).

    Returns:
        bool: True if the files were uploaded successfully, False otherwise.
    """
    try:

        s3_client = boto3.client('s3', 
                            region_name='us-west-2', 
                            aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
                            aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'))

        # s3_client = boto3.client('s3')

        for root, dirs, files in os.walk(directory):
            for file in files:
                file_path = os.path.join(root, file)
                s3_key = os.path.relpath(file_path, directory).replace(os.sep, '/')
                s3_key = os.path.join(os.path.basename(directory), s3_key)

                if include_extensions:
                    file_extension = os.path.splitext(file_path)[1]
                    if file_extension not in include_extensions:
                        continue

                if exclude_extensions:
                    file_extension = os.path.splitext(file_path)[1]
                    if file_extension in exclude_extensions:
                        continue

                print('upload_directory_to_s3: file_extension: ',  os.path.splitext(file_path)[1])
                print('upload_directory_to_s3: file_path: ', file_path)
                print('upload_directory_to_s3: bucket_name: ', bucket_name)
                print('upload_directory_to_s3: s3_key: ', s3_key)

                s3_client.upload_file(file_path, bucket_name, s3_key)

        return True
    except Exception as e:
        print(f"Failed to upload directory to S3: {e}")
        return False
    
# def upload_file_to_s3(file_path, bucket_name, s3_key):
#     s3_client = boto3.client('s3', 
#                              region_name='us-west-2', 
#                              aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
#                              aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'))

#     # s3_key = TOPICS_DIR + '/' + TOPIC_STRING_DASH + '.txt'

#     print('s3_key: ', s3_key)

#     s3_client.upload_file(filename, TOPICS_BUCKET, s3_key, ExtraArgs={'ACL':'public-read'})