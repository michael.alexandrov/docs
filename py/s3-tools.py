import os
import json
import argparse
import boto3
from modules.utils.Storage import Storage

"""
python s3-tools.py --bucket my-bucket --operation upload --local-path /path/to/local/directory --s3-prefix my-folder --include .txt --exclude .bak
python s3_operations.py --bucket my-bucket --operation download --local-path /path/to/local/directory --s3-prefix my-folder --include .jpg --exclude .tmp
python s3_operations.py --bucket my-bucket --operation delete --s3-prefix my-folder --include .log --exclude .zip

"""

def upload_to_s3(bucket_name, local_path, s3_prefix=None, include=None, exclude=None):
    """
    Uploads files from a local directory to an S3 bucket.

    Args:
        bucket_name (str): The name of the S3 bucket.
        local_path (str): The local directory path.
        s3_prefix (str): The prefix to add to S3 object keys (optional).
        include (str): Filter files to include based on prefix, suffix, or substring (optional).
        exclude (str): Filter files to exclude based on prefix, suffix, or substring (optional).
    """
    s3_client = boto3.client('s3')

    print('upload_to_s3 bucket_name: ', bucket_name)
    print('upload_to_s3 local_path: ', local_path)
    print('upload_to_s3 s3_prefix: ', s3_prefix)

    for root, dirs, files in os.walk(local_path):
        for file_name in files:
            print('upload_to_s3 file_name: ', file_name)
            file_path = os.path.join(root, file_name)
            s3_key = os.path.relpath(file_path, local_path) #.replace(os.sep, '/')
            if s3_prefix:
                s3_key = os.path.join(s3_prefix, s3_key)

            s3_key = s3_key.replace(os.sep, '/')

            if include and not (include in file_name or file_name.startswith(include) or file_name.endswith(include)):
                continue

            if exclude and (exclude in file_name or file_name.startswith(exclude) or file_name.endswith(exclude)):
                continue

            print('upload_to_s3 after filter: ', file_name)
            print('upload_to_s3 after filter s3_key: ', s3_key)

            if file_name.endswith('.html'):
                s3_client.upload_file(file_path, bucket_name, s3_key, ExtraArgs={'ContentType': "text/html", 'ACL': "public-read"})
            elif file_name.endswith('.js'):
                s3_client.upload_file(file_path, bucket_name, s3_key, ExtraArgs={'ContentType': "application/javascript", 'ACL': "public-read"})
            elif file_name.endswith('.json'):
                s3_client.upload_file(file_path, bucket_name, s3_key, ExtraArgs={'ContentType': "application/json", 'ACL': "public-read"})
            else:
                s3_client.upload_file(file_path, bucket_name, s3_key)


def download_from_s3(bucket_name, local_path, s3_prefix=None, include=None, exclude=None):
    """
    Downloads files from an S3 bucket to a local directory.

    Args:
        bucket_name (str): The name of the S3 bucket.
        local_path (str): The local directory path.
        s3_prefix (str): The prefix used in S3 object keys (optional).
        include (str): Filter files to include based on prefix, suffix, or substring (optional).
        exclude (str): Filter files to exclude based on prefix, suffix, or substring (optional).
    """
    s3_client = boto3.client('s3')

    response = s3_client.list_objects_v2(Bucket=bucket_name, Prefix=s3_prefix)

    for item in response.get('Contents', []):
        s3_key = item['Key']

        print('download_from_s3 s3_key: ', s3_key)
        if include and not (include in s3_key or s3_key.startswith(include) or s3_key.endswith(include)):
            continue

        if exclude and (exclude in s3_key or s3_key.startswith(exclude) or s3_key.endswith(exclude)):
            continue

        file_path = os.path.join(local_path, os.path.basename(s3_key))

        print('download_from_s3 file_path: ', file_path)

        s3_client.download_file(bucket_name, s3_key, file_path)


def delete_from_s3(bucket_name, s3_prefix=None, include=None, exclude=None):
    """
    Deletes objects from an S3 bucket.

    Args:
        bucket_name (str): The name of the S3 bucket.
        s3_prefix (str): The prefix used in S3 object keys (optional).
        include (str): Filter objects to include based on prefix, suffix, or substring (optional).
        exclude (str): Filter objects to exclude based on prefix, suffix, or substring (optional).
    """
    s3_client = boto3.client('s3')

    response = s3_client.list_objects_v2(Bucket=bucket_name, Prefix=s3_prefix)

    objects_to_delete = {
        'Objects': [{'Key': item['Key']} for item in response.get('Contents', []) if not (
            (include and not (include in item['Key'] or item['Key'].startswith(include) or item['Key'].endswith(include))) or
            (exclude and (exclude in item['Key'] or item['Key'].startswith(exclude) or item['Key'].endswith(exclude))))
        ]
    }

    if objects_to_delete['Objects']:
        s3_client.delete_objects(Bucket=bucket_name, Delete=objects_to_delete)


def get_s3_bucket_objects_list(bucket_name, include=None, exclude=None):
    """
    Retrieves a list of S3 bucket objects with optional include and exclude filters.

    Args:
        bucket_name (str): The name of the S3 bucket.
        include (str): Filter objects to include based on prefix, suffix, or substring (optional).
        exclude (str): Filter objects to exclude based on prefix, suffix, or substring (optional).

    Returns:
        str: JSON array of objects containing information about the S3 bucket objects.
    """
    s3_client = boto3.client('s3')

    response = s3_client.list_objects_v2(Bucket=bucket_name)

    objects = []
    files_count = 0
    for item in response.get('Contents', []):
        s3_key = item['Key']

        # include in s3_key or s3_key.startswith(include) or

        if include and not ( s3_key.endswith(include)):
            continue

        if exclude and (exclude in s3_key or s3_key.startswith(exclude) or s3_key.endswith(exclude)):
            continue

        files_count += 1
        object_info = {
            'id': files_count,
            'name': s3_key,
            'size': item['Size'],
            'date': item['LastModified'].isoformat()
        }

        objects.append(object_info)

    return objects

def main():
    parser = argparse.ArgumentParser(description='S3 Object Operations')
    parser.add_argument('--bucket', required=True, help='S3 bucket name')
    parser.add_argument('--operation', choices=['upload', 'download', 'delete', 'update_list'], required=True, help='S3 operation')
    parser.add_argument('--local-path', required=True, help='Local directory path')
    parser.add_argument('--s3-prefix', help='Prefix for S3 object keys')
    parser.add_argument('--include', help='Filter files/objects to include')
    parser.add_argument('--exclude', help='Filter files/objects to exclude')

    args = parser.parse_args()

    bucket_name = args.bucket
    local_path = args.local_path
    s3_prefix = args.s3_prefix
    include = args.include
    exclude = args.exclude

    storage = Storage()

    if args.operation == 'upload':
        upload_to_s3(bucket_name, local_path, s3_prefix, include, exclude)
    elif args.operation == 'download':
        download_from_s3(bucket_name, local_path, s3_prefix, include, exclude)
    elif args.operation == 'delete':
        delete_from_s3(bucket_name, s3_prefix, include, exclude)
    elif args.operation == 'update_list':
        objects_list = get_s3_bucket_objects_list(bucket_name, include, exclude)
        print('objects_list: ', objects_list)
        storage.write_json(local_path, objects_list)
        


if __name__ == '__main__':
    main()

