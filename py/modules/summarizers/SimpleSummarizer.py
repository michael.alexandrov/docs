import nltk
import pandas as pd
import heapq
# from nltk.sentiment import SentimentIntensityAnalyzer
from modules.utils.Storage import Storage

class SimpleSummarizer:
    def __init__(self, target_csv, target_column, ngram_csv, ngram_col_name='str'):
        self.target_csv = target_csv
        self.target_column = target_column
        self.ngram_csv = ngram_csv
        self.ngram_col_name = ngram_col_name
        self.storage = Storage()
        self.target_df = self.storage.csv_to_df(self.target_csv)
        self.ngram_df = self.storage.csv_to_df(self.ngram_csv)
        self.word_frequencies = {}
        # self.max_row_len = self.target_df[self.target_column ].str.split(' ').str.len().max()
        

    def calculate_frequencies(self):
        max_freq = 1
        if len(self.ngram_df['count']) > 0:
            max_freq = max(self.ngram_df['count'])
    
        for index, ngram_row in self.ngram_df.iterrows():
            self.word_frequencies[ngram_row[self.ngram_col_name]] = (ngram_row['count'] / max_freq)

        # print('calculate_frequencies', self.word_frequencies)


    def get_score(self, row):
        score = 0
        words = nltk.word_tokenize(row[self.target_column].lower())
        words_count = len(words)
        for word in words:
            if word in self.word_frequencies.keys():
                score += self.word_frequencies[word] 

        # score = score * (words_count  / self.max_row_len) #self.max_row_len / words_count)
        return pd.Series([score])


    def add_scores(self, score_col_name='simple_score', csv_suffix=''):
        print('SimpleSummarizer.target_csv: ', self.target_csv)
        print('SimpleSummarizer.add_scores target_column: ', self.target_column)
        print('SimpleSummarizer.add_scores csv_suffix: ', csv_suffix)

        self.calculate_frequencies()
        
        self.target_df[[score_col_name]] = self.target_df.apply(self.get_score, axis=1)
        # self.target_df.sort_values(by=['simple_score' ], ascending=[False], inplace=True)

        if csv_suffix:
            self.target_csv = self.target_csv.replace('.csv', csv_suffix)

        print('SimpleSummarizer.add_scores save to csv_path: ', self.target_csv)
        self.target_df.to_csv(self.target_csv)


    def update_summary(self, summary_path, n_top):
        # top_scored = heapq.nlargest(n_top, scores, key=scores.get)

        self.target_df.sort_values(by=[self.target_column], inplace=True)
        self.target_df = self.target_df.drop_duplicates(subset=[self.target_column])
        self.target_df.sort_index(inplace=True)

        top_df = self.target_df.nlargest(n_top,'simple_score')
            
        summary = '\n============\n'.join(top_df[self.target_column])
        self.storage.write_file(summary_path, summary)
        
