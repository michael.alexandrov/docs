from sumy.parsers.plaintext import PlaintextParser
from sumy.nlp.tokenizers import Tokenizer
from sumy.summarizers.lsa import LsaSummarizer as Summarizer
from sumy.nlp.stemmers import Stemmer
from sumy.utils import get_stop_words

"""
https://pypi.org/project/sumy/

https://ijedr.org/papers/IJEDR2101019.pdf

Sumy is a python package for automatic summarization of text documents and HTML pages.It uses multiple algorithms and
methods for summarization.
1) USING LEX-RANK [29]
It is an unsupervised approach to summarise text focused on graph-based centrality scoring of sentences. It uses the approach
of finding similar sentences which will likely be of great importance. To install use pip install lexrank.
For ex:- # Using LexRank
summarizer = LexRankSummarizer()
#Summarize the document with 2 sentences
summary = summarizer(parser.document, 2)
2) USING LUHN [28]
Luhn is a heuristic method for summarising text. It is based on the frequency of most important words.
For ex:- from sumy.summarizers.luhn import LuhnSummarizer
summarizer_luhn = LuhnSummarizer()
summary_1 =summarizer_luhn(parser.document,2)
3) USING LSA[18[
LSA stands for Latent Semantic Analysis. It is uses term frequency techniques with a singular value break up to summarize
text.
For ex:- from sumy.summarizers.lsa import LsaSummarizer
summarizer_lsa = LsaSummarizer()
summary_2 =summarizer_lsa(parser.document,2)

"""

def get_sumy_summary(text, sentences_count):
    LANGUAGE = "english"
    
    parser = PlaintextParser.from_string(text, Tokenizer(LANGUAGE))
    stemmer = Stemmer(LANGUAGE)

    summarizer = Summarizer(stemmer)
    summarizer.stop_words = get_stop_words(LANGUAGE)

    results = []
    for sentence in summarizer(parser.document, sentences_count):
        # print('get_sumy_summary:', sentence)
        results.append(str(sentence))

    return results
       