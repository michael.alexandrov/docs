from pysummarization.nlpbase.auto_abstractor import AutoAbstractor
from pysummarization.tokenizabledoc.simple_tokenizer import SimpleTokenizer
from pysummarization.abstractabledoc.top_n_rank_abstractor import TopNRankAbstractor

"""
https://pypi.org/project/pysummarization/
"""

def get_py_summary(text, sentences_count):
    auto_abstractor = AutoAbstractor()
    auto_abstractor.tokenizable_doc = SimpleTokenizer()
    auto_abstractor.delimiter_list = ['.', '\n']
    abstractable_doc = TopNRankAbstractor()
    abstractable_doc.set_top_n(sentences_count)
    result_dict = auto_abstractor.summarize(text, abstractable_doc)

    results = []
    for sentence in result_dict['summarize_result']:
        # print('get_py_summary:', sentence)
        results.append(sentence)

    return results