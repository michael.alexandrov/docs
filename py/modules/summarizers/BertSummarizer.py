from summarizer import Summarizer

"""
https://pypi.org/project/bert-extractive-summarizer/#simple-example
"""
def get_bert_summary(text, num_sentences):
    model = Summarizer()
    result = model(text, num_sentences=num_sentences)
    return result
    
