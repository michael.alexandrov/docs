import nltk
import pandas as pd
from lib.utils.Storage import Storage
from lib.utils.TextUtils import *


class MultiSrcSummarizer:
    def __init__(self, topic_dir, target_csv, target_column):
        self.storage = Storage()
        self.topic_dir = topic_dir
        self.target_csv = target_csv
        self.target_df = self.storage.csv_to_df(self.target_csv)
        self.target_column = target_column
        self.word_frequencies = {}
        self.src_count = {}
        self.src_length = 10

    def calculate_frequencies(self):
        src_list = get_sources_list(self.topic_dir)
        self.src_length = len(src_list)

        for src in src_list:
            ngram_df = self.storage.csv_to_df(get_file_path(self.topic_dir, src, 'text/ngram-1.csv'))
            max_freq = 1
            if len(ngram_df['count']) > 0:
                max_freq = max(ngram_df['count'])

            for index, ngram_row in ngram_df.iterrows():
                if ngram_row['str'] in self.word_frequencies:
                    self.word_frequencies[ngram_row['str']] += (ngram_row['count'] / max_freq)
                else:
                    self.word_frequencies[ngram_row['str']] = (ngram_row['count'] / max_freq)

                if ngram_row['str'] in self.src_count:
                    self.src_count[ngram_row['str']] += 1
                else:
                    self.src_count[ngram_row['str']] = 1

        # print('calculate_frequencies self.word_frequencies: ', self.word_frequencies)
        # print('calculate_frequencies self.src_count: ', self.src_count)


    def get_score(self, row):
        score = 0
        for word in nltk.word_tokenize(row[self.target_column].lower()):
            if word in self.word_frequencies.keys():
                score += self.word_frequencies[word] * (self.src_count[word] / self.src_length)

        return pd.Series([score])

    
    def add_scores(self, csv_suffix=''):
        print('MultiSrcSummarizer.target_csv: ', self.target_csv)
        print('MultiSrcSummarizer.add_scores target_column: ', self.target_column)
        print('MultiSrcSummarizer.add_scores csv_suffix: ', csv_suffix)

        self.calculate_frequencies()
        
        self.target_df[['multi_score']] = self.target_df.apply(self.get_score, axis=1)
        # self.target_df.sort_values(by=['multi_score' ], ascending=[False], inplace=True)

        if csv_suffix:
            self.target_csv = self.target_csv.replace('.csv', csv_suffix)

        print('MultiSrcSummarizer add_scores save to csv_path: ', self.target_csv)
        self.target_df.to_csv(self.target_csv)


    def update_summary(self, summary_path, n_top):
        # top_scored = heapq.nlargest(n_top, scores, key=scores.get)
        top_df = self.target_df.nlargest(n_top,'multi_score')
        summary = ''

        for index, row in top_df.iterrows():
            summary += row[self.target_column]
            summary += '\n<a href="' + row['src'] + '">' + row['title'] + '</a>\n\n'

        # summary = '\n============\n'.join(top_df[self.target_column])
        self.storage.write_file(summary_path, summary)