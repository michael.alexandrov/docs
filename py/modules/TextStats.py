import os
import pandas as pd
import spacy
import pytextrank

from modules.utils.Storage import Storage
from modules.SpacyProcessor import SpacyProcessor
from modules.utils.TextUtils import get_file_path, get_file_name_from_url


class TextStats:
    def __init__(self, config={}, lang='EN') -> None:
        self.storage = Storage()
        self.config = config

        self.columns = ['text', 'lemma_', 'ent_type_', 'pos_', 'tag_', 'dep_', 'norm_', 'ent_iob_', 'shape_', 'is_stop']

        if self.config.get('tables') and self.config['tables'].get('pos') and self.config['tables']['pos'].get('columns'):
            self.columns = self.config['tables']['pos']['columns']

        self.nlp_model = 'en_core_web_lg'
        if lang.upper() == 'RU':
            self.nlp_model = 'ru_core_news_lg'
        
        self.nlp = spacy.load(self.nlp_model)
        self.nlp.max_length = 3000000
        # self.nlp.add_pipe("merge_entities")
        # self.nlp.add_pipe("merge_noun_chunks")
        self.nlp.add_pipe("textrank", last=True)


    # def get_config_val(self, path):
    #     arr = path.split('/')
    #     obj = self.config.get(arr[0])
    #     for i in range(1, len(arr) - 1):
    #         print('get_config_val obj: ', obj)
    #         print('get_config_val obj type: ', type(obj))
    #         if obj:
    #             print('get_config_val elem: ', arr[i])

    #             obj = obj.get(arr[i])
                
    #         else:
    #             break
    #     return obj

    def load_config(self, config_path=''):
        if not config_path or len(config_path) == 0:
            config_path = 'config/default.yaml'
            
        self.config = self.storage.read_yaml(config_path)
        print('load_config config: ', self.config)

        
    def update(self):

        # self.create_sents_csv(self.config['text_path'])
        
        
        # self.spacy_create_stats(self.config['text_path'], lang=self.config['lang'])

        sp = SpacyProcessor(config=self.config, lang=self.config['lang'])
        
        if self.config.get('tables') and self.config['tables'].get('pos'):

            pos_csv_path = self.config['text_path'] + self.config['tables']['pos']['suffix'] #'.pos.csv' #os.path.join(file_stats_dir, 'spacy-pos.csv.csv')
            print('pos_csv_path: ', pos_csv_path)

            # sp.update_pos_csv(self.storage.read_file(self.config['text_path']), pos_csv_path)
            # sp.update_pos_count(pos_csv_path)

            # self.column_value_counts(pos_csv_path,'text')
            # self.column_value_counts(pos_csv_path,'lemma_')
            # self.column_value_counts(pos_csv_path,'pos_')
            # self.column_value_counts(pos_csv_path,'is_stop')


            # filter = {
            #     'pos_': 'NOUN' ,
            #     'is_stop': 'True'
            # }

            # self.column_value_counts(pos_csv_path,'text', {'pos_': 'NOUN'}, '.NOUN.count.csv' )
            # self.column_value_counts(pos_csv_path,'lemma_', {'pos_': 'NOUN'}, '.NOUN.count.csv')

            # sents_csv_path =     'C:\\data\\books\\SEMIOTIKA\\part1.txt.sents.csv'
            # pos_count_csv_path = 'C:\\data\\books\\SEMIOTIKA\\part1.txt.pos.csv.lemma_.NOUN.count.csv'

            sents_csv_path =     'C:\\data\\books\\JUNG\\psytypes.txt.sents.csv'
            pos_count_csv_path = 'C:\\data\\books\\JUNG\\psytypes.txt.pos.csv.lemma_.NOUN.count.csv'
           
            # self.pos_md(sents_csv_path, pos_count_csv_path)

            self.sents_md(sents_csv_path, pos_count_csv_path)



    def create_sents_csv(self, text_path):
        suffix = self.config['tables']['sentences']['suffix'] #'.sents.csv'
        csv_path = text_path + suffix
        print('create_sents_csv config: ', csv_path)
        df = self.create_sents_df(self.storage.read_file(text_path))
        df.to_csv(csv_path)


    def create_sents_df(self, text):
        doc = self.nlp(text)
        sents = []
        columns = ['index', 'sentence']

        if self.config.get('tables') and self.config['tables'].get('sentences') and self.config['tables']['sentences'].get('columns'):
            columns = self.config['tables']['sentences']['columns']

        print('create_sents_df columns: ', columns)

        for sent_i, sent in enumerate(doc.sents):
            sent_row = [sent_i, sent]

            #TODO: Handle column order !!!
            if 'chars' in columns:
                sent_row.append(len(sent))

            print(sent_row)
            sents.append(sent_row)

        return pd.DataFrame(data=sents,columns=columns)


    def get_lines_with_tokens(self, lines, tokens):
        res_arr = []

        for line in lines:
            match_count = 0
            for token in tokens:
                if line.count(token) > 0:
                    match_count += 1
            if match_count == len(tokens):
                res_arr.append(line)

        return res_arr


    def add_md_links(self, txt, tokens):
        for token in tokens:
            txt = txt.replace(token, '[[' + token + ']]')
        return txt


    def column_value_counts_df(self, csv_path, column, filter={}):

        print('column_value_counts_df csv_path:', csv_path)
        print('column_value_counts_df column:', column)
        print('column_value_counts_df filter:', filter)
        df = self.storage.csv_to_df(csv_path)

        print('column_value_counts_df df count:', df[column].count())

        # for column in df.columns:
        # df_count = df[column].value_counts()

        # filter = {
        #     'pos_': 'NOUN',
        #     'is_stop': 'False'
        # }

        df_filtered = df
        for col in filter.keys():
            print('column_value_counts_df filter col:', col)
            print('column_value_counts_df filter col filter:', filter[col])
            # print('column_value_counts_df df_filtered count before:', df_filtered[column].count())
            # # df_filtered = df_filtered[df_filtered[column] == filter[col]]
            # print('column_value_counts_df df_filtered count after:', df_filtered[column].count())

            df_filtered = df_filtered[df_filtered[col] == filter[col]]

        df_filtered = pd.DataFrame(df_filtered[column].value_counts(), columns = [column, 'count'])

        # df_count.to_csv(csv_path + '.' + column + '.count.csv')
        return df_filtered


    def column_value_counts(self, csv_path, column, filter={}, sufix='.count.csv'):
        # df = self.storage.csv_to_df(csv_path)
        # # for column in df.columns:
        # df_count = df[column].value_counts()
        print('column_value_counts filter: ', filter)
        df_count = self.column_value_counts_df(csv_path, column, filter)
        print('column_value_counts df_count count:', df_count[column].count())

        col_count_csv_path = csv_path + '.' + column + sufix
        print('column_value_counts col_count_csv_path: ', col_count_csv_path)

        df_count.to_csv(col_count_csv_path)


    def lemma_pos_count(self, pos_csv_path):
        tokens_df = self.storage.csv_to_df(pos_csv_path)

        max_rows_count = 20
        # for index, row in tokens_df.iterrows():


    def ngrams_md(self, text_path, ngram_csv_path):
        
        lines = self.storage.get_file_lines(text_path )

        ngram_df = self.storage.csv_to_df(ngram_csv_path)

        max_rows_count = 20
        for index, row in ngram_df.iterrows():
            txt = row['str']
            file_path = os.path.join(self.config['data_path'], row['str'] + '.md')
            # if not any(tag in row['str'] for tag in ignore_tags):

            token_lines_arr = self.get_lines_with_tokens(lines,[row['str']])  
            txt = '\n'.join(token_lines_arr)
            txt = self.add_md_links(txt, ngram_df.head(20)['str'])
            self.storage.write_file(file_path,txt)

            if index > max_rows_count:
                break


    def spacy_create_stats(self, text_path, lang='EN'):
        full_path = os.path.abspath(text_path)
        print('full_path: ', full_path)

        file_name = full_path.replace(os.path.dirname(full_path) + os.sep,'')
        print('file_name: ', file_name)

        file_stats_dir = os.path.join(os.path.dirname(text_path), self.config['path_prefix'], get_file_name_from_url(file_name))
        print('file_stats_dir: ', file_stats_dir)

        sentences_csv_path = os.path.join(file_stats_dir, 'sentences.csv')

        print('sentences_csv_path: ', sentences_csv_path)

        dir_path = os.path.dirname(sentences_csv_path)
        print('dir_path: ', dir_path)

        if not os.path.exists(dir_path):
            os.makedirs(dir_path)


        sp = SpacyProcessor(lang=lang)
        # sp.update_sentences(sentences_csv_path, self.storage.read_file(text_path))

        pos_csv_path = full_path + '.pos.csv' #os.path.join(file_stats_dir, 'spacy-pos.csv.csv')
        print('pos_csv_path: ', pos_csv_path)

        sp.update_pos_csv(self.storage.read_file(full_path), pos_csv_path)
        sp.update_pos_count(pos_csv_path)


    def get_sents_with_lemma(self, sents_arr, lemma):
        res_arr = []
        for sent in sents_arr:
            # match_count = 0
            doc = self.nlp(sent)
            for token in doc:
                if getattr(token, 'lemma_') == lemma:
                    res_arr.append(sent)
                    break
        return res_arr


    def pos_md(self, sents_csv_path, pos_count_csv_path):
        print('pos_md sents_csv_path: ', sents_csv_path)
        print('pos_md pos_count_csv_path: ', pos_count_csv_path)
        
        sents = self.storage.csv_to_df(sents_csv_path)['sentence']
        pos_count_df = self.storage.csv_to_df(pos_count_csv_path)

        # file_name = full_path.replace(os.path.dirname(full_path) + os.sep,'')
        # os.path.dirname(pos_count_csv_path)
        md_dir = os.path.dirname(pos_count_csv_path)

        if not os.path.exists(md_dir):
            os.makedirs(md_dir)

        print('pos_md md_dir: ', md_dir)

        top_lemmas_md = '' 

        count_col_name = pos_count_df.columns[0]
        print('pos_md count_col_name: ', count_col_name)
        

        for index, row in pos_count_df.head(20).iterrows():
            # txt = row['token']
            top_lemmas_md += '\n[[' + row[count_col_name] + ']]'
            file_path = os.path.join(md_dir, row[count_col_name] + '.md')
            print('pos_md token file_path: ', file_path)
            # if not any(tag in row['str'] for tag in ignore_tags):

            lemma_sents_arr = self.get_sents_with_lemma(sents, row[count_col_name])  
            print('pos_md token lemma_sents_arr len: ', len(lemma_sents_arr))

            txt = '\n\n'.join(lemma_sents_arr)
            txt = self.add_md_links(txt, pos_count_df.head(20)[count_col_name])
            self.storage.write_file(file_path,txt)

    # def append_sent_md(self, token, sent):

    def sents_md(self, sents_csv_path, pos_count_csv_path):
        print('pos_md sents_csv_path: ', sents_csv_path)
        print('pos_md pos_count_csv_path: ', pos_count_csv_path)
        
        sents_df = self.storage.csv_to_df(sents_csv_path)
        # pos_count_df = self.storage.csv_to_df(pos_count_csv_path)

        # file_name = full_path.replace(os.path.dirname(full_path) + os.sep,'')
        # os.path.dirname(pos_count_csv_path)
        # md_dir = os.path.dirname(pos_count_csv_path)
        file_path = ''
        for index, row in sents_df.head(150).iterrows():
            # txt = row['token']
            # top_lemmas_md += '\n[[' + row[count_col_name] + ']]'

            sent_file_name = '.sent-' + str(index) + '.md'
            prev_file_name = '.sent-' + str(index - 1) + '.md'
            next_file_name = '.sent-' + str(index + 1) + '.md'
            sent_file_path = sents_csv_path + sent_file_name

            prev_file_path = sents_csv_path + prev_file_name
            next_file_path = sents_csv_path + next_file_name

            print('sents_md token file_path: ', sent_file_path)

            sent_txt = row['sentence']

            doc = self.nlp(sent_txt)
            doc_len = len(doc)
            # print('doc len:', doc_len)

            print('>>> sents_md doc_len: ' + str(doc_len) + ' sent_txt: ', sent_txt)

            if doc_len > 5:
                for token in doc:
                    token_text = getattr(token, 'text')
                    token_pos = getattr(token, 'pos_')
                    if getattr(token, 'is_stop') or token_pos in ['PUNCT', 'SPACE']:
                        continue

                    token_text = token_text.replace('/','-')
                    
                    
                    lemma = getattr(token, 'lemma_').replace('/','-')
                    print(token_text, token_pos, lemma)

                    if token_pos in ['NOUN', 'VERB', 'ADJ']:
                        #TODO: replace specifiic instnce by position
                        sent_txt = sent_txt.replace(token_text, '[' + token_text + '](' + lemma + '.md)' )

                        lemma_md_path = os.path.join(os.path.dirname(sents_csv_path), lemma + '.md')

                        lemma_md = ''
                        if os.path.exists(lemma_md_path):
                            lemma_md = self.storage.read_file(lemma_md_path)
                        
                        

                        lemma_md += '/n[' + sent_file_path + '](' + sent_file_path + ')\n' + sent_txt

                        print('lemma_md_path: ', lemma_md_path)
                        print('lemma_md', lemma_md)
                        
                        self.storage.write_file(lemma_md_path, lemma_md)


                        # [My Link](My%20Link.md)
            sent_txt = '[[' + prev_file_path + ']] [[' + next_file_path + ']]\n' + sent_txt
            # lemma_md

            print('### sents_md sent_txt: ', sent_txt)
            self.storage.write_file(sent_file_path, sent_txt)



            
            # if not any(tag in row['str'] for tag in ignore_tags):

            # lemma_sents_arr = self.get_sents_with_lemma(sents, row[count_col_name])  
            # print('pos_md token lemma_sents_arr len: ', len(lemma_sents_arr))

            # txt = '\n\n'.join(lemma_sents_arr)
            # txt = self.add_md_links(txt, pos_count_df.head(20)[count_col_name])
            # self.storage.write_file(file_path,txt)

            # md_txt = 