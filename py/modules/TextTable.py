
import spacy
import pandas as pd
from modules.utils.Storage import Storage
import pytextrank

class TextTable:
    def __init__(self, config={}, lang='EN') -> None:
        self.storage = Storage()
        self.config = config
        self.columns = ['text', 'lemma_', 'ent_type_', 'pos_', 'tag_', 'dep_', 'norm_', 'ent_iob_', 'shape_', 'is_stop']

        self.nlp_model = 'en_core_web_lg'
        if lang.upper() == 'RU':
            self.nlp_model = 'ru_core_news_lg'
        
        self.nlp = spacy.load(self.nlp_model)
        # self.nlp.add_pipe("merge_entities")
        # self.nlp.add_pipe("merge_noun_chunks")
        self.nlp.add_pipe("textrank", last=True)

        

    def _get_row(self, token):
        vals = []
        for column in self.columns:
            vals.append(getattr(token, column))
        return vals

    def create_table(self, text_path):

        doc = self.nlp(self.storage.read_file(text_path))
        arr = []
        for token in doc:
            arr.append(self._get_row(token))

        print('TextTable.create_table columns: ', self.columns)
        df = pd.DataFrame(data=arr,columns=self.columns)
        df.to_csv(text_path.replace('.txt','.csv'))

        

    def column_value_counts(self, csv_path):
        df = self.storage.csv_to_df(csv_path)
        for column in df.columns:
            df_count = df[column].value_counts()
            df_count.to_csv(csv_path.replace('.csv', '_' + column + '_count.csv'))


    def get_chunks(self, text_path):
        doc = self.nlp(self.storage.read_file(text_path))
        # arr = []
        # for chunk in doc.noun_chunks:
        #     # arr.append(self._get_row(token))
        #     print(chunk.text, chunk.root.text, chunk.root.dep_, chunk.root.head.text)

        for p in doc._.phrases:
            # phrases_arr.append({'text': p.text, 'count': p.count, 'rank': p.rank})
            print({'text': p.text, 'count': p.count, 'rank': p.rank})

    def get_sentences(self, text_path):
        doc = self.nlp(self.storage.read_file(text_path))
        sents = []
        for sent_i, sent in enumerate(doc.sents):
            print('======================')
            print(sent)
            print('======================')
            sents.append([sent_i, sent])