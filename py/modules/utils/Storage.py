#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import pandas as pd
import json
from typing import Dict, List, Any, Union
import yaml
import fnmatch
import csv


'''
Sources
rules
actions

'''

class Storage:
    es_docs = []

    def read_file(self,path):
        f = open(path, "r", encoding="utf8")
        text = f.read()
        f.close()
        return text
    
    def get_file_lines(self,path):
        f = open(path, "r", encoding="utf8")
        lines = f.read().splitlines()
        f.close()
        return lines

    def write_file(self, path, text):
        f = open(path, "wb")
        f.write(text.encode("utf8"))
        f.close()

    def write_csv(self, path, df):
        pass

    def read_csv(self, path):
        pass

    def read_json(self,path):
        f = open(path, encoding="utf8")
        obj = json.load(f)
        f.close()
        return obj

    def write_json(self,path,obj):
        json_file = open(path, 'w', encoding="utf8")
        json.dump(obj, json_file, indent=4)
        json_file.close()

    def read_yaml(self,path):
        f = open(path, encoding="utf8")
        obj = yaml.load(f, Loader=yaml.FullLoader)
        f.close()
        return obj

    def write_yaml(self,path,obj):
        yaml_file = open(path, 'w', encoding="utf8")
        yaml.dump(obj, stream=yaml_file,
                       default_flow_style=False, sort_keys=False)
        yaml_file.close()
 
    def csv_to_df(self, csv_path):
        return pd.read_csv(csv_path, na_filter = False)
    

    def find_files(self, path, file_pattern):
        """
        Recursively iterates through a folder and returns a list of the full paths to files that match a specified file pattern.

        Args:
        path: The path of the folder to search.
        file_pattern: The file pattern to match (e.g. "*.txt").

        Returns:
        A list of full paths to files that match the specified file pattern.
        """

        # Create an empty list to store the full paths to matching files.
        matching_files = []

        # Recursively iterate through the folder.
        for dirpath, dirnames, filenames in os.walk(path):
            # Iterate through the filenames in the current directory.
            for filename in filenames:
                # If the filename matches the specified file pattern, add the full path to the list of matching files.
                if fnmatch.fnmatch(filename, file_pattern):
                    matching_files.append(os.path.join(dirpath, filename))

        return matching_files
    
    def csv_to_json(self, csv_path, json_path=''):
        if not json_path:
            json_path = csv_path.replace('.csv', '.json')

        print('csv_to_json csv_path: ', csv_path)
        print('csv_to_json json_path: ', json_path)

        json_array = []

        with open(csv_path, 'r', encoding="utf8") as file:
            reader = csv.DictReader(file)
            for row in reader:
                json_array.append(row)

        self.write_json(json_path, json_array)