import os
import re
import string
import pandas as pd
from operator import itemgetter
import nltk
from nltk.corpus import stopwords
from nltk import word_tokenize, BigramCollocationFinder, BigramAssocMeasures

import spacy
from collections import defaultdict


pos_mapping = {
    'CC': 'coordinating conjunction',
    'CD': 'cardinal digit',
    'DT': 'determiner',
    'EX': 'existential there',
    'FW': 'foreign word',
    'IN': 'preposition/subordinating conjunction',
    'JJ': 'adjective big',
    'JJR': 'adjective, comparative bigger',
    'JJS': 'adjective, superlative biggest',
    'LS': 'list marker 1)',
    'MD': 'modal could, will',
    'NN': 'noun, singular',
    'NNS': 'noun plural',
    'NNP': 'proper noun, singular',
    'NNPS': 'proper noun, plural',
    'PDT': 'predeterminer',
    'POS': 'possessive ending',
    'PRP': 'personal pronoun',
    'PRP$': 'possessive pronoun',
    'RB': 'adverb very, silently',
    'RBR': 'adverb, comparative',
    'RBS': 'adverb, superlative',
    'RP': 'particle',
    'TO': 'to go to',
    'UH': 'interjection',
    'VB': 'verb, base form',
    'VBD': 'verb, past tense',
    'VBG': 'verb, gerund/present participle',
    'VBN': 'verb, past participle taken',
    'VBP': 'verb, sing. present',
    'VBZ': 'verb, 3rd person sing',
    'WDT': 'wh-determiner',
    'WP': 'wh-pronoun',
    'WP$': 'possessive',
    'WRB': 'wh-abverb'
}


def get_path(data_dir, file_name):
    file_path = os.path.join(data_dir, file_name)
    if not os.path.isdir( os.path.dirname(file_path)):
        os.makedirs(os.path.dirname(file_path))
    return file_path


def get_clean_text(text):
    text = re.sub('[\W+ ]',' ', text)
    while '  ' in text:
        text = text.replace('  ', ' ')
    return text


def get_clean_tokens(text, language='english'):
    text = get_clean_text(text)
    # print(text)
    tokens = nltk.word_tokenize(text.lower(), language=language)

    stop_words = nltk.corpus.stopwords.words(language)

    filtered_tokens = []
    for token in tokens:
        if len(token) > 2 and token not in stop_words:
            filtered_tokens.append(token)

    return filtered_tokens

    
# def get_clean_text(text):
#     regex = re.compile('[^a-zA-Z0-9 ]')
#     return regex.sub(' ', text)


def remove_short_lines(text, min_len=5):
    cleaned_text = ''
    lines = text.split('\n')
    for line in lines:
        if len(get_clean_text(line).split(' ')) > min_len:
           cleaned_text += line + '\n'

    return cleaned_text


# def get_clean_tokens(text):
#     text = get_clean_text(text)
#     tokens = word_tokenize(text.lower())

#     stop_words = stopwords.words('english')

#     filtered_tokens = []
#     for token in tokens:
#         if len(token) > 2 and token not in stop_words:
#             filtered_tokens.append(token)

#     return filtered_tokens


def get_file_name_from_url(url):
    name = url.lower().replace('https://', '').replace('http://', '').rstrip('/')
    regex = re.compile('[^a-zA-Z0-9 ]')
    name = regex.sub('_', name)
    return name


def get_file_path(base_dir, url, name):
    file_path = os.path.join(base_dir, get_file_name_from_url(url), name)
    dir_path = os.path.dirname(file_path)
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    return file_path


def get_sources_list(base_dir):
    sources_df = pd.read_csv(os.path.join(base_dir, 'sources.csv'))
    return sources_df['href']


def get_ngram_count(text,length,min_count=2):
    filtered_tokens = get_clean_tokens(text)
    ngram_count = nltk.FreqDist(nltk.ngrams(filtered_tokens, length))
    ngram_count = sorted(ngram_count.items(), key=itemgetter(1), reverse=True)
    ngram_count = list(filter(lambda x: x[1] >= min_count, ngram_count))
    return ngram_count

def get_ngram_count_df(text,length,min_count=2):
    ngrams_count = get_ngram_count(text,length,min_count)
    ngrams_arr = []
    for ngram in ngrams_count:
        ngrams_arr.append({'str': ' '.join(ngram[0]).replace('"', '\\"'), 'count': ngram[1]})

    ngrams_df = pd.DataFrame(ngrams_arr, columns=['str', 'count'])
    return ngrams_df

def get_ngram_count_dict(text,length,min_count=2):
    filtered_tokens = get_clean_tokens(text)
    ngram_count = nltk.FreqDist(nltk.ngrams(filtered_tokens, length))
    ngram_count = sorted(ngram_count.items(), key=itemgetter(1), reverse=True)
    ngram_count = list(filter(lambda x: x[1] >= min_count, ngram_count))

    ngrams_dict = {}
    for ngram in ngram_count:
        ngrams_dict[' '.join(ngram[0]).replace('"','\\"')] = ngram[1]
    return ngrams_dict #dict((word, freq) for word, freq in ngram_count if freq >= min_count)



def get_ngrams(text):
    text = text.lower().replace('\n', ' ')
    regex = re.compile('[^a-zA-Z ]')
    text = regex.sub('', text)

    #tokens = word_tokenize(text)

    ngrams_count = []
    for i in range(2,6):
        ngrams_count += get_ngram_count(text,i) # nltk.ngrams(tokens, i)

    print(ngrams_count)
    # fdist = nltk.FreqDist(ngrams_count)
    # print(fdist.most_common(50))

def get_char_gram_count(text,length,min_count=2):
    # filtered_tokens = get_clean_tokens(text)
    ngram_count = nltk.FreqDist(nltk.ngrams(text, length))
    ngram_count = sorted(ngram_count.items(), key=itemgetter(1), reverse=True)
    ngram_count = list(filter(lambda x: x[1] >= min_count, ngram_count))
    return ngram_count

def get_char_gram_count_df(text,length,min_count=2):
    ngrams_count = get_char_gram_count(text,length,min_count)
    ngrams_arr = []
    for ngram in ngrams_count:
        ngrams_arr.append({'str': ''.join(ngram[0]).replace('"', '\\"'), 'count': ngram[1]})

    ngrams_df = pd.DataFrame(ngrams_arr, columns=['str', 'count'])
    return ngrams_df

def get_colocations(text, num, window_size):

    tokens = word_tokenize(text)
    ignored_words = stopwords.words('english')
    finder = BigramCollocationFinder.from_words(tokens, window_size)
    finder.apply_freq_filter(2)
    finder.apply_word_filter(lambda w: len(w) < 3 or w.lower() in ignored_words)
    bigram_measures = BigramAssocMeasures()
    collocations = finder.nbest(bigram_measures.likelihood_ratio, num)
    colloc_strings = [w1 + ' ' + w2 for w1, w2 in collocations]

    return colloc_strings


def get_scored(arr, ngram_df):
    max_freq = 1
    if len(ngram_df['count']) > 0:
        max_freq = max(ngram_df['count'])

    word_frequencies = {}
    for link_index, ngram_row in ngram_df.iterrows():
        word_frequencies[ngram_row['str']] = (ngram_row['count'] / max_freq)

    scores = {}
    for item in arr:
        for word in nltk.word_tokenize(item.lower()):
            if word in word_frequencies.keys():
                if len(item.split(' ')) > 3 and len(item.split(' ')) < 50:
                    if item not in scores.keys():
                        scores[item] = word_frequencies[word]
                    else:
                        scores[item] += word_frequencies[word]
    return scores


def remove_punctuation(s):
    # Remove punctuation
    s = s.translate(str.maketrans("", "", string.punctuation))
    
    # Remove white spaces
    s = "".join(s.split())
    
    # Remove new lines
    s = s.replace("\n", "")
    
    return s



def detect_language(s):
    s = remove_punctuation(s)
    # print('detect_language: ', len(s), s)
    if len(s) == 0:
        return None
    # Define the ranges for the average values of Unicode numeric values
    english_range = (50, 150)
    spanish_range = (200, 300)
    german_range = (400, 500)
    russian_range = (1000, 1100)
    ukrainian_range = (1100, 1200)
    hebrew_range = (1400, 1500)

    # Calculate the average value of Unicode numeric values for the input string
    avg_value = sum(ord(c) for c in s.replace(' ', '')) / len(s)

    # print('avg_value: ', avg_value)

    # Determine the language based on the average value range
    if avg_value >= english_range[0] and avg_value <= english_range[1]:
        return "en"
    elif avg_value >= spanish_range[0] and avg_value <= spanish_range[1]:
        return "es"
    elif avg_value >= german_range[0] and avg_value <= german_range[1]:
        return "de"
    elif avg_value >= russian_range[0] and avg_value <= russian_range[1]:
        return "ru"
    elif avg_value >= ukrainian_range[0] and avg_value <= ukrainian_range[1]:
        return "uk"
    elif avg_value >= hebrew_range[0] and avg_value <= hebrew_range[1]:
        return "he"
    else:
        return None
    

def nltk_dependency_parser(text):
    # Tokenize the text
    tokens = nltk.word_tokenize(text)

    # Create a dependency parser object
    parser = nltk.parse.corenlp.CoreNLPDependencyParser()

    # Parse the text and extract dependencies
    parsed = parser.parse(tokens)
    dependencies = list(parsed.dependencies)

    # Create a dictionary to store the results
    results = defaultdict(dict)

    # Extract subject, verb, and object from each dependency
    for dependency in dependencies:
        rel, gov, dep = dependency[0], dependency[1], dependency[2]
        if rel == 'nsubj':
            subject = gov[0]
            if dep[1] in ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']:
                verb = dep[0]
                obj = ''
                for sub_dep in dependencies:
                    if sub_dep[0] == 'dobj' and sub_dep[1][0] == dep[2]:
                        obj = sub_dep[2][0]
                        break
                results[subject][verb] = obj

    # Return the results
    return dict(results)


def spacy_dependency_parser(text):
    # Load a spaCy English language model
    nlp = spacy.load('en_core_web_lg')

    # Parse the text with the model
    doc = nlp(text)

    # Create a dictionary to store the results
    results = defaultdict(dict)

    # Extract subject, verb, and object from each dependency
    for token in doc:
        if token.dep_ == 'nsubj':
            subject = token.text
            verb = token.head.text
            obj = ''
            for child in token.head.children:
                if child.dep_ == 'dobj':
                    obj = child.text
                    break
            results[subject][verb] = obj

    # Return the results
    return dict(results)