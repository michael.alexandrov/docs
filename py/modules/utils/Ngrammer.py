import re
import pandas as pd
from operator import itemgetter
import nltk

from lib.utils.TextUtils import *

# def get_clean_text(text):
#     text = re.sub('[\W+ ]',' ', text)
#     while '  ' in text:
#         text = text.replace('  ', ' ')
#     return text


# def get_clean_tokens(text, language='english'):
#     text = get_clean_text(text)
#     print(text)
#     tokens = nltk.word_tokenize(text.lower(), language=language)

#     stop_words = nltk.corpus.stopwords.words(language)

#     filtered_tokens = []
#     for token in tokens:
#         if len(token) > 2 and token not in stop_words:
#             filtered_tokens.append(token)

#     return filtered_tokens

class Ngrammer:
    def __init__(self, text, language='english', doCleanTokens=True, multigramWithSpaces=True):
        self.text = text
        self.doCleanTokens = doCleanTokens
        self.multigramWithSpaces = multigramWithSpaces

        self.tokens = self.text
        if self.doCleanTokens:
            self.tokens = get_clean_tokens(self.tokens, language=language)

    def get_ngram_count(self, length, min_count=2):
        ngram_count = nltk.FreqDist(nltk.ngrams(self.tokens, length))
        ngram_count = sorted(ngram_count.items(), key=itemgetter(1), reverse=True)
        ngram_count = list(filter(lambda x: x[1] >= min_count, ngram_count))
        # print('get_ngram_count: ', ngram_count)
        return ngram_count

    def get_ngram_count_df(self, length,min_count=2):
        ngrams_count = self.get_ngram_count(length,min_count)
        ngrams_arr = []

        for ngram in ngrams_count:
            if self.multigramWithSpaces:
                str = ' '.join(ngram[0]).replace('"', '\\"')
            else:
                str = ''.join(ngram[0]).replace('"', '\\"')

            ngrams_arr.append({'str': str, 'count': ngram[1]})

        ngrams_df = pd.DataFrame(ngrams_arr, columns=['str', 'count'])
        return ngrams_df