import time

class Tracer(object):
    def __init__(self, f):
        self.f = f

    def __call__(self, *args):
        start = time.time()
        print(self.f.__name__, ' : ', args[0])

        self.f(*args)
        end = time.time()
        print(self.f.__name__, ' duration: ', str(end - start))