import os
import sys
import pandas as pd
import spacy
import pytextrank
from spacytextblob.spacytextblob import SpacyTextBlob
#python -m textblob.download_corpora

from modules.utils.Storage import Storage
from modules.utils.TextUtils import *

from modules.summarizers.SimpleSummarizer import SimpleSummarizer
from modules.utils.Storage import Storage

"""
https://downloads.cs.stanford.edu/nlp/software/dependencies_manual.pdf
https://universaldependencies.org/en/dep/
"""

class SpacyProcessor:
    def __init__(self, config=None, lang='EN'):
        self.storage = Storage()
        self.config=config

        # if not self.config:
        #     self.config = self.config = self.storage.read_yaml('config/default.yaml')

        self.columns = ['i', 'text', 'lemma_', 'ent_type_', 'pos_', 'tag_', 'dep_', 'norm_', 'ent_iob_']
        self.storage = Storage()

        self.nlp_model = 'en_core_web_lg'
        if lang.upper() == 'RU':
            self.nlp_model = 'ru_core_news_lg'
        
        self.nlp = spacy.load(self.nlp_model)
        self.nlp.add_pipe("spacytextblob")

        self.nlp.max_length = 3000000
        self.nlp.add_pipe("textrank")
        
        
    def get_row(self, token, columns):
        vals = []
        for column in columns:
            vals.append(getattr(token, column))
        return vals


    def update_sentences(self, csv_path, text='', save_json=True):
        print('update_sentences: ', csv_path)
        if text:
            self.text = text
            self.nlp.max_length = len(text)
            self.doc = self.nlp(self.text)

        sents = []
        for sent_i, sent in enumerate(self.doc.sents):
            # print('sent: ', sent)
            polarity = 0.0
            subjectivity = 0.0
            # if isinstance(sent, str) and len(sent.strip()) > 0:
            # d = self.nlp(sent)
            polarity =  sent._.blob.polarity
            subjectivity = sent._.blob.subjectivity

            sents.append([sent,polarity,subjectivity])

        df = pd.DataFrame(data=sents,columns=['sentence', 'polarity','subjectivity'])
        df.index.name = 'id'
        
        df.to_csv(csv_path)

        if save_json:
            self.storage.csv_to_json(csv_path)
            # print('df.to_json', df.to_json(orient='records', default_handler=str))
            # arr = df.to_json(csv_path.replace('.csv','.json'), orient='records', default_handler=str)
            # print('save_json:', arr)
            # self.storage.write_json(csv_path.replace('.csv','.json'), arr)


    def update_pos_csv(self, text, csv_path, save_json=True):
        self.text = text
        self.doc = self.nlp(self.text)

        arr = []
        columns = self.columns

        for token in self.doc:
            arr.append(self.get_row(token, columns))

        df = pd.DataFrame(data=arr,columns=columns)
        df.to_csv(csv_path)

        if save_json:
            self.storage.csv_to_json(csv_path)


    def update_pos_count(self, pos_csv_path):
        print('update_pos_count: ', pos_csv_path)
        exclude_ent_types = [] #['CARDINAL', 'DATE', 'MONEY', 'ORDINAL', 'PERCENT', 'TIME']
        nsubj_count = {}
        adj_count = {}
        pobj_dobj_count = {}

        pos_df = self.storage.csv_to_df(pos_csv_path)
        for index, row in pos_df.iterrows():
            # print(row)
            #Nsubj Nouns count
            if row['dep_'] == 'nsubj' and row['pos_'] == 'NOUN' and row['ent_type_'] not in exclude_ent_types:
                if row['lemma_'] in nsubj_count.keys():
                    nsubj_count[row['lemma_']] +=1
                else:
                    nsubj_count[row['lemma_']] =1

            #Adjectives count
            if row['pos_'] == 'ADJ' and row['ent_type_'] not in exclude_ent_types:
                if row['lemma_'] in adj_count.keys():
                    adj_count[row['lemma_']] +=1
                else:
                    adj_count[row['lemma_']] =1

            #Pobj, Dobj count
            if row['dep_'] in ['pobj', 'dobj'] and row['tag_'] in ['NN', 'NNP', 'NNS', 'NNPS']:
                if row['ent_type_'] not in exclude_ent_types:
                    if row['lemma_'] in pobj_dobj_count.keys():
                        pobj_dobj_count[row['lemma_']] +=1
                    else:
                        pobj_dobj_count[row['lemma_']] =1

        nsubj_df = pd.DataFrame(nsubj_count.items(), columns=['token', 'count'])
        nsubj_df.sort_values(by=['count'], ascending=[False], inplace=True)
        nsubj_df.to_csv(pos_csv_path.replace('.csv','-nsubj-count.csv'))

        adj_df = pd.DataFrame(adj_count.items(), columns=['token', 'count'])
        adj_df.sort_values(by=['count'], ascending=[False], inplace=True)
        adj_df.to_csv(pos_csv_path.replace('.csv','-adj-count.csv'))

        obj_df = pd.DataFrame(pobj_dobj_count.items(), columns=['token', 'count'])
        obj_df.sort_values(by=['count'], ascending=[False], inplace=True)
        obj_df.to_csv(pos_csv_path.replace('.csv','-obj-count.csv'))


storage = Storage()

def spacy_update_source(topic_path, src):
    sp = SpacyProcessor()
    sp.update_pos_csv(storage.read_file(get_file_path(topic_path, src, 'text/text.txt')),
                                        get_file_path(topic_path, src, 'text/spacy-pos.csv'))
    sp.update_pos_count(get_file_path(topic_path, src, 'text/spacy-pos.csv'))

    adjSum = SimpleSummarizer(get_file_path(topic_path, src, 'text/sentences.csv'),'sentences',
                            get_file_path(topic_path, src, 'text/spacy-pos-adj-count.csv'),'token')
    adjSum.add_scores('adj_score','-spacy-score.csv')


    subjSum = SimpleSummarizer(get_file_path(topic_path, src, 'text/sentences-spacy-score.csv'),'sentences',
                                get_file_path(topic_path, src, 'text/spacy-pos-nsubj-count.csv'),'token')
    subjSum.add_scores('subj_score')


    objSum = SimpleSummarizer(get_file_path(topic_path, src, 'text/sentences-spacy-score.csv'),'sentences',
                                get_file_path(topic_path, src, 'text/spacy-pos-obj-count.csv'),'token')
    objSum.add_scores('obj_score')

def spacy_update_topic(topic_path):
    spacy_update_source(topic_path, '_all')
    for src in get_sources_list(topic_path):
        spacy_update_source(topic_path, src)
