import os
import pandas as pd
import heapq
from modules.utils.Tracer import Tracer
from modules.utils.TextUtils import *
from modules.utils.Storage import Storage
# from modules.sentiment.NltkSia import NltkSia
# from modules.sentiment.PatternSentiment import PatternSentiment

# from modules.summarizers.SimpleSummarizer import SimpleSummarizer
# from modules.summarizers.MultiSrcSummarizer import MultiSrcSummarizer
# from modules.summarizers.SumySummarizer import get_sumy_summary
# from modules.summarizers.PySummarizer import get_py_summary
# from modules.summarizers.BertSummarizer import get_bert_summary

# from modules.SnippetsProcessor import SnippetsProcessor

from wordcloud import WordCloud

# from modules.SpacyProcessor import spacy_update_topic


class TextProcessor:
    def __init__(self, content_dir):
        self.content_dir = content_dir
        self.storage = Storage()

#    @Tracer
    def aggregate_text(self):
        print('aggregate_text')

        all_text = ""
        all_links_text = ""
        all_images_text = ""

        for src in get_sources_list(self.content_dir):
            print('aggregate_text: ', src)

            all_text += self.storage.read_file(get_file_path(self.content_dir, src, 'text/text.txt')) + '\n'

            links_df = self.storage.csv_to_df(get_file_path(self.content_dir, src, 'links/links.csv'))
            for link_index, link_row in links_df.iterrows():
                if link_row['text'] and len(link_row['text']) > 2:
                    all_links_text += link_row['text'] + '\n'

            images_df = self.storage.csv_to_df(get_file_path(self.content_dir, src, 'images/images.csv'))
            for img_index, img_row in images_df.iterrows():
                # print('img_row', img_row['alt'])
                if img_row['alt'] and len(img_row['alt']) > 2:
                    all_images_text += img_row['alt'] + '\n'
                
        self.storage.write_file(get_file_path(self.content_dir, '_all', 'text/text.txt'), all_text)
        self.storage.write_file(get_file_path(self.content_dir, '_all', 'links/text.txt'), all_links_text)
        self.storage.write_file(get_file_path(self.content_dir, '_all', 'images/text.txt'), all_images_text)

        self.update_wordcloud(all_text, get_file_path(self.content_dir, '_all', 'summary/all-text-wordcloud.png'))
        self.update_wordcloud(all_links_text, get_file_path(self.content_dir, '_all', 'summary/all-links-text-wordcloud.png'))
        self.update_wordcloud(all_images_text, get_file_path(self.content_dir, '_all', 'summary/all-images-text-wordcloud.png'))

#    @Tracer
    def update_file_pos(self, file_path):
        tokens = word_tokenize(self.storage.read_file(file_path))
        tagged = nltk.pos_tag(tokens)

        pos_df = pd.DataFrame(tagged, columns=['token', 'pos'])
        pos_df.to_csv(file_path + '.pos.csv')

#    @Tracer
    def update_pos(self):
        print('update_topic_pos self.content_dir: ', self.content_dir)

        for src in get_sources_list(self.content_dir):
            self.update_file_pos(get_file_path(self.content_dir, src, 'text/text.txt'))

#    @Tracer
    def update_pos_count(self):
        pos_count = {}
        for src in get_sources_list(self.content_dir):
            pos_df= pd.read_csv(get_file_path(self.content_dir, src, 'text/pos.csv'))
            for index, row in pos_df.iterrows():
                token = str(row['token']).lower()
                if row['pos'] in pos_count:   
                    if token in pos_count[row['pos']]:
                        pos_count[row['pos']][token] += 1
                    else:
                        pos_count[row['pos']][token] = 1
                else:
                    pos_count[row['pos']] = {}
                    pos_count[row['pos']][token] = 1

        pos_count_arr = []

        for pos in pos_count:
            for token in pos_count[pos]:
                pos_count_arr.append([pos, token, pos_count[pos][token] ])

        pos_count_df = pd.DataFrame(pos_count_arr, columns=['pos', 'token', 'count']) 
        pos_count_df.sort_values(by=['pos', 'count'], ascending=[True, False], inplace=True)
        pos_count_df.to_csv(os.path.join(self.content_dir, '_all/text/pos_count.csv'))

#    @Tracer
    def update_file_colocations(self, file_path):
        print('update_file_colocations: ', file_path)
        text = self.storage.read_file(file_path)
        for i in range(2, 4):
            # print(i)
            cols = get_colocations(text, 50, i)
            # cols = get_colocations(self.storage.read_file(file_path), 250, 8)
            cols_df = pd.DataFrame(cols, columns=['str'])
            cols_df.to_csv(file_path.replace('text.txt', 'cols-50-' + str(i) + '.csv'))

#    @Tracer
    def update_colocations(self):
        # print('update_colocations: ', file_path)
        for src in get_sources_list(self.content_dir):
            print('update_topic_colocations href:', src)
            self.update_file_colocations(get_file_path(self.content_dir, src, 'text/text.txt'))

        self.update_file_colocations(get_file_path(self.content_dir, '_all', 'text/text.txt'))
        self.update_file_colocations(get_file_path(self.content_dir, '_all', 'links/text.txt'))
        self.update_file_colocations(get_file_path(self.content_dir, '_all', 'images/text.txt'))

#    @Tracer
    def update_file_ngrams(self, file_path, save_json=False):
        print('update_file_ngrams: ', file_path)

        ngrams_df = get_ngram_count_df(self.storage.read_file(file_path),1,1)
        ngram_path = file_path + '.ngram-1.csv'
        ngrams_df.to_csv(ngram_path)

        if save_json:
            self.storage.csv_to_json(ngram_path)

        for i in range(2, 6):
            ngrams_df = get_ngram_count_df(self.storage.read_file(file_path),i,2)
            ngram_path = file_path +  '.ngram-' + str(i) + '.csv'
            ngrams_df.to_csv(ngram_path)
            if save_json:
                self.storage.csv_to_json(ngram_path)

#    @Tracer
    def update_ngrams(self):   
        # sources_df = pd.read_csv(os.path.join(self.content_dir, 'sources.csv')) 
        for src in get_sources_list(self.content_dir):
            print('update_ngrams href:', src)
            self.update_file_ngrams(get_file_path(self.content_dir, src, 'text/text.txt'))

        self.update_file_ngrams(get_file_path(self.content_dir, '_all', 'text/text.txt'))
        self.update_file_ngrams(get_file_path(self.content_dir, '_all', 'links/text.txt'))
        self.update_file_ngrams(get_file_path(self.content_dir, '_all', 'images/text.txt'))

    def update_file_char_grams(self, file_path):
        print('update_file_char_grams: ', file_path)

        ngrams_df = get_char_gram_count_df(self.storage.read_file(file_path),1,1)
        ngram_path = os.path.join(self.content_dir, file_path.replace('text.txt', 'char-1.csv'))
        ngrams_df.to_csv(ngram_path)

        for i in range(2, 7):
            ngrams_df = get_char_gram_count_df(self.storage.read_file(file_path),i,2)
            ngram_path = os.path.join(self.content_dir, file_path.replace('text.txt', 'char-' + str(i) + '.csv'))
            ngrams_df.to_csv(ngram_path)

    def update_char_grams(self):   
        # sources_df = pd.read_csv(os.path.join(self.content_dir, 'sources.csv')) 
        for src in get_sources_list(self.content_dir):
            print('update_ngrams href:', src)
            self.update_file_char_grams(get_file_path(self.content_dir, src, 'text/text.txt'))

        self.update_file_char_grams(get_file_path(self.content_dir, '_all', 'text/text.txt'))
        self.update_file_char_grams(get_file_path(self.content_dir, '_all', 'links/text.txt'))
        self.update_file_char_grams(get_file_path(self.content_dir, '_all', 'images/text.txt'))

    def update_sentences(self):
        sources_df = pd.read_csv(os.path.join(self.content_dir, 'sources.csv'))
        sentences_frames = []
        lines_frames = []
        for index, row in sources_df.iterrows():
            print('sources_df: ', row['text'])
            print('sources_df: ', row['href'])

            sentences = []
            # sentences = nltk.sent_tokenize(self.storage.read_file(get_file_path(self.content_dir, row['href'], 'text/text.txt')))
            # sentences_df = pd.DataFrame(sentences, columns=['sentences'])
            # sentences_df.sort_values(by=['sentences'], inplace=True)
            # sentences_df = sentences_df.drop_duplicates(subset=['sentences'])
            # sentences_df.sort_index(inplace=True)
            
            # sentences_df['title'] = row['text']
            # sentences_df['src'] = row['href']

            # sentences_path = get_file_path(self.content_dir, row['href'], 'text/sentences.csv')
            # sentences_df.to_csv(sentences_path)
            # sentences_frames.append(sentences_df)

            lines = self.storage.get_file_lines(get_file_path(self.content_dir, row['href'], 'text/text.txt'))
            for line in lines:
                sentences += nltk.sent_tokenize(line)
            
            sentences_df = pd.DataFrame(sentences, columns=['sentences'])
            sentences_df['title'] = row['text']
            sentences_df['src'] = row['href']
            sentences_path = get_file_path(self.content_dir, row['href'], 'text/sentences.csv')
            sentences_df.to_csv(sentences_path)
            sentences_frames.append(sentences_df)

            lines_df = pd.DataFrame(lines, columns=['lines'])
            # print('lines_df:', lines_df)
            lines_df.sort_values(by=['lines'], inplace=True)
            lines_df = lines_df.drop_duplicates(subset=['lines'])
            lines_df.sort_index(inplace=True)
            lines_df['title'] = row['text']
            lines_df['src'] = row['href']
            lines_path = get_file_path(self.content_dir, row['href'], 'text/lines.csv')
            lines_df.to_csv(lines_path)
            lines_frames.append(lines_df)

        all_sentences_df = pd.concat(sentences_frames)
        all_sentences_df.sort_values(by=['sentences'], inplace=True)
        all_sentences_df = all_sentences_df.drop_duplicates(subset=['sentences'])
        all_sentences_df.sort_index(inplace=True)
        all_sentences_df.to_csv(get_file_path(self.content_dir, '_all', 'text/sentences.csv'))

        all_lines_df = pd.concat(lines_frames)
        all_lines_df.sort_values(by=['lines'], inplace=True)
        all_lines_df = all_lines_df.drop_duplicates(subset=['lines'])
        all_lines_df.sort_index(inplace=True)
        all_lines_df.to_csv(get_file_path(self.content_dir, '_all', 'text/lines.csv'))

#    @Tracer
    def add_sentiment_scores(self, csv_path, scored_column):
        try:
            nltkSia = NltkSia() 
            nltkSia.add_polarity_scores(csv_path, scored_column)
            ps = PatternSentiment()
            ps.add_sentiment_scores(csv_path, scored_column)
        except Exception as e:
            print('Exception in add_sentiment_scores src: ', csv_path)
            print(f"Unexpected {e=}, {type(e)=}")
            print("Oops!", e.__class__, "occurred.")

#    @Tracer
    def update_sentiment_scores(self):

        self.add_sentiment_scores(get_file_path(self.content_dir, '_all', 'text/lines.csv'),'lines')
        self.add_sentiment_scores(get_file_path(self.content_dir, '_all', 'text/sentences.csv'),'sentences')
        # self.add_sentiment_scores(get_file_path(self.content_dir, '_all', 'links.csv'),'text')
        # self.add_sentiment_scores(get_file_path(self.content_dir, '_all', 'images.csv'),'alt')

        for src in get_sources_list(self.content_dir):
            print('update_sentiment_scores href:', src)
            self.add_sentiment_scores(get_file_path(self.content_dir, src, 'text/lines.csv'),'lines')
            self.add_sentiment_scores(get_file_path(self.content_dir, src, 'text/sentences.csv'),'sentences')


    def update_wordcloud(self, text, image_path):
        print('update_wordcloud: ', image_path)
        wordcloud = WordCloud().generate(text)
        wordcloud.to_file(image_path)


    def update_pos_wordcloud(self):
        all_nouns = ''
        all_verbs = ''
        all_adjectives = ''

        pos_nouns = ['NN','NNS','NNP','NNPS']
        pos_verbs = ['VB','VBD','VBG','VBN','VBP','VBZ']
        pos_adjectives = ['JJ', 'JJR', 'JJS']
    
        for src in get_sources_list(self.content_dir):
            print('update_pos_wordcloud: ', src)
            pos_df = self.storage.csv_to_df(get_file_path(self.content_dir, src, 'text/pos.csv'))
            if len(pos_df['pos'].tolist()) == 0:
                print('update_pos_wordcloud: empty pos.csv', src)
                continue

            pos_df = pos_df[pos_df['token'].apply(lambda x: len(x) > 2)]
            pos_df = pos_df[pos_df['pos'].apply(lambda x: len(x) > 1)]

            nouns_list = pos_df.loc[pos_df['pos'].isin(pos_nouns)]['token'].tolist()
            nouns = ' '.join(nouns_list)
            if len(nouns) > 0:
                self.update_wordcloud(nouns, get_file_path(self.content_dir, src, 'summary/nouns-wordcloud.png'))
                all_nouns += ' ' + nouns
       
            verbs_list = pos_df.loc[pos_df['pos'].isin(pos_verbs)]['token'].tolist()
            verbs = ' '.join(verbs_list)
            if len(verbs) > 0:
                self.update_wordcloud(verbs, get_file_path(self.content_dir, src, 'summary/verbs-wordcloud.png'))
                all_verbs += ' ' + verbs
        
            adjectives_list = pos_df.loc[pos_df['pos'].isin(pos_adjectives)]['token'].tolist()
            adjectives = ' '.join(adjectives_list)
            if len(adjectives) > 0:
                self.update_wordcloud(adjectives, get_file_path(self.content_dir, src, 'summary/adjectives-wordcloud.png'))
                all_adjectives += ' ' + adjectives

        if len(all_nouns) > 0:
            self.update_wordcloud(all_nouns, get_file_path(self.content_dir, '_all', 'summary/all-nouns-wordcloud.png'))

        if len(all_verbs) > 0:
            self.update_wordcloud(all_verbs, get_file_path(self.content_dir, '_all', 'summary/all-verbs-wordcloud.png'))

        if len(all_adjectives) > 0:
            self.update_wordcloud(all_adjectives, get_file_path(self.content_dir, '_all', 'summary/all-adjectives-wordcloud.png'))
        

#    @Tracer
    def update_text_summary(self, src, n_top=15):
        try:
            print('update_text_summary src: ', src)
            simpleSum = SimpleSummarizer(get_file_path(self.content_dir,src,'text/sentences.csv'), 'sentences',
            get_file_path(self.content_dir,src,'text/ngram-1.csv'))
            simpleSum.add_scores()
            simpleSum.update_summary(get_file_path(self.content_dir,src,'summary/sents-summary.txt'), n_top)

            simpleSum = SimpleSummarizer(get_file_path(self.content_dir,src,'text/lines.csv'), 'lines',
            get_file_path(self.content_dir,src,'text/ngram-1.csv'))
            simpleSum.add_scores()
            simpleSum.update_summary(get_file_path(self.content_dir,src,'summary/lines-summary.txt'), n_top)

            # sumy_sum = get_sumy_summary(self.storage.read_file(get_file_path(self.content_dir,src,'text.txt')), n_top)
            # self.storage.write_file(get_file_path(self.content_dir,src,'_sumy-summary.txt'), '\n\n'.join(sumy_sum))

            # py_sum = get_py_summary(self.storage.read_file(get_file_path(self.content_dir,src,'text.txt')), n_top)
            # self.storage.write_file(get_file_path(self.content_dir,src,'_py-summary.txt'), '\n\n'.join(py_sum))

            # bert_sum = get_bert_summary(self.storage.read_file(get_file_path(self.content_dir,src,'text.txt')), n_top)
            # self.storage.write_file(get_file_path(self.content_dir,src,'_bert-summary.txt'), bert_sum)
        except Exception as e:
            print('Exception in update_text_summary src: ', src)
            print(f"Unexpected {e=}, {type(e)=}")
            print("Oops!", e.__class__, "occurred.")
          

#    @Tracer
    def update_summary(self):

        self.update_text_summary('_all', 15)

        mss = MultiSrcSummarizer(self.content_dir, get_file_path(self.content_dir,'_all', 'text/lines.csv'), 'lines')
        mss.add_scores('-multi.csv')
        mss.update_summary(get_file_path(self.content_dir, '_all', 'summary/lines-multi-summary.txt'), 15)

        mss = MultiSrcSummarizer(self.content_dir, get_file_path(self.content_dir,'_all', 'text/sentences.csv'), 'sentences')
        mss.add_scores('-multi.csv')
        mss.update_summary(get_file_path(self.content_dir, '_all', 'summary/sentences-multi-summary.txt'), 15)

        
        # self.update_text_summary(get_file_path(self.content_dir, '_all', 'links-text.txt'), 
        #                         get_file_path(self.content_dir, '_all', 'links-ngram-1.csv'))

        # self.update_text_summary(get_file_path(self.content_dir, '_all', 'images-text.txt'), 
        #                         get_file_path(self.content_dir, '_all', 'images-ngram-1.csv'))

      

        for src in get_sources_list(self.content_dir):
            self.update_text_summary(src, 15)
            # self.update_text_summary(get_file_path(self.content_dir, src, 'text.txt'), 
            #                     get_file_path(self.content_dir, src, 'ngram-1.csv'))

            # self.update_text_summary(get_file_path(self.content_dir, src, 'links-text.txt'), 
            #                         get_file_path(self.content_dir, src, 'links-ngram-1.csv'))

            # self.update_text_summary(get_file_path(self.content_dir, src, 'images-text.txt'), 
            #                         get_file_path(self.content_dir, src, 'images-ngram-1.csv'))
    
    def remove_short_lines(self):
        for src in get_sources_list(self.content_dir):
            print('remove_short_lines href:', src)
            text = self.storage.read_file(get_file_path(self.content_dir, src, 'text/text.txt'))
            self.storage.write_file(get_file_path(self.content_dir, src, 'text/text-full.txt'), text)
            text = remove_short_lines(text, 5)
            self.storage.write_file(get_file_path(self.content_dir, src, 'text/text.txt'), text)


    def update(self):
        # self.remove_short_lines()
        self.aggregate_text()
        self.update_colocations()
        self.update_pos()
        self.update_pos_count()
        self.update_ngrams()
        self.update_char_grams()
        self.update_sentences()
        self.update_pos_wordcloud()
        self.update_summary()
        self.update_sentiment_scores()
        spacy_update_topic(self.content_dir)

        snipsProc = SnippetsProcessor(self.content_dir)
        snipsProc.update_snippets()