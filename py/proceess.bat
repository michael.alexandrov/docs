python -V

set TOPICS_DIR=topics
set TOPICS_BUCKET=michael.alexandrov

set DOCS_DIR=C:\dev\gitlab\docs

@REM mkdir %TOPICS_DIR%

@REM python s3-tools.py --bucket %TOPICS_BUCKET% --operation upload --local-path %DOCS_DIR% --s3-prefix "" --include .html
python s3-tools.py --bucket %TOPICS_BUCKET% --operation upload --local-path %DOCS_DIR%/js --s3-prefix js --include .js


@REM python s3-tools.py --bucket %TOPICS_BUCKET% --operation download --local-path %TOPICS_DIR% --s3-prefix %TOPICS_DIR% --include .txt
@REM python s3-tools.py --bucket %TOPICS_BUCKET% --operation update_list --local-path %TOPICS_DIR%/files-list.json --s3-prefix %TOPICS_DIR% --include .txt


@REM python modules/utils/init_nltk.py

@REM python -m spacy download en_core_web_lg
@REM python -m spacy download ru_core_news_lg

@REM python txt-sents.py %TOPICS_DIR% 
@REM python s3-tools.py --bucket %TOPICS_BUCKET% --operation upload --local-path %TOPICS_DIR% --s3-prefix %TOPICS_DIR% --include .json
@REM python s3-tools.py --bucket %TOPICS_BUCKET% --operation upload --local-path %TOPICS_DIR% --s3-prefix %TOPICS_DIR% --include .csv

