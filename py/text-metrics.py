import os
import re
import glob
from collections import Counter


def get_word_count(directory, file_filter="*.txt"):
    """
    Calculates the total word count in the files within a directory.

    Args:
        directory (str): The directory path.
        file_filter (str): File filter pattern (default: "*.txt").

    Returns:
        int: The total word count.
    """
    word_count = 0

    file_paths = glob.glob(os.path.join(directory, file_filter))

    for file_path in file_paths:
        with open(file_path, "r") as file:
            content = file.read()
            words = re.findall(r"\b\w+\b", content)
            word_count += len(words)

    return word_count


def get_vocabulary_size(directory, file_filter="*.txt"):
    """
    Calculates the vocabulary size (count of unique words) in the files within a directory.

    Args:
        directory (str): The directory path.
        file_filter (str): File filter pattern (default: "*.txt").

    Returns:
        int: The vocabulary size.
    """
    vocabulary = set()

    file_paths = glob.glob(os.path.join(directory, file_filter))

    for file_path in file_paths:
        with open(file_path, "r") as file:
            content = file.read()
            words = re.findall(r"\b\w+\b", content)
            vocabulary.update(words)

    return len(vocabulary)


def get_term_frequency(directory, file_filter="*.txt"):
    """
    Calculates the term frequency (TF) of each term in the files within a directory.

    Args:
        directory (str): The directory path.
        file_filter (str): File filter pattern (default: "*.txt").

    Returns:
        dict: A dictionary mapping each term to its frequency count.
    """
    term_frequency = Counter()

    file_paths = glob.glob(os.path.join(directory, file_filter))

    for file_path in file_paths:
        with open(file_path, "r") as file:
            content = file.read()
            words = re.findall(r"\b\w+\b", content)
            term_frequency.update(words)

    return dict(term_frequency)


def get_document_frequency(directory, file_filter="*.txt"):
    """
    Calculates the document frequency (DF) of each term in the files within a directory.

    Args:
        directory (str): The directory path.
        file_filter (str): File filter pattern (default: "*.txt").

    Returns:
        dict: A dictionary mapping each term to its document frequency count.
    """
    document_frequency = Counter()

    file_paths = glob.glob(os.path.join(directory, file_filter))

    for file_path in file_paths:
        with open(file_path, "r") as file:
            content = file.read()
            words = re.findall(r"\b\w+\b", content)
            document_frequency.update(set(words))

    return dict(document_frequency)


# Other text metric functions...


def main():
    directory = os.getenv('TOPICS_DIR')
    os.makedirs(directory, exist_ok=True)
    file_filter = "*.txt"

    word_count = get_word_count(directory, file_filter)
    print("Word Count:", word_count)

    vocabulary_size = get_vocabulary_size(directory, file_filter)
    print("Vocabulary Size:", vocabulary_size)

    term_frequency = get_term_frequency(directory, file_filter)
    print("Term Frequency len:", len(term_frequency))

    document_frequency = get_document_frequency(directory, file_filter)
    print("Document Frequency len:", len(document_frequency))


if __name__ == "__main__":
    main()
